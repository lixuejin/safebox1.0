package cn.bingoogolapple.baseadapter;

public class BR {
  public static final int _all = 0;

  public static final int model = 1;

  public static final int onClick = 2;

  public static final int statusModel = 3;

  public static final int uiHandler = 4;

  public static final int viewHolder = 5;
}
