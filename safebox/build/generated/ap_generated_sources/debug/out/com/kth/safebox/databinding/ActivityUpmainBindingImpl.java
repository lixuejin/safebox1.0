package com.kth.safebox.databinding;
import com.kth.safebox.R;
import com.kth.safebox.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityUpmainBindingImpl extends ActivityUpmainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_face_title, 5);
        sViewsWithIds.put(R.id.constraintLayout2, 6);
        sViewsWithIds.put(R.id.upname, 7);
        sViewsWithIds.put(R.id.upmacname, 8);
        sViewsWithIds.put(R.id.mymood, 9);
        sViewsWithIds.put(R.id.updevice, 10);
        sViewsWithIds.put(R.id.rcydevice, 11);
        sViewsWithIds.put(R.id.rcydevice1, 12);
        sViewsWithIds.put(R.id.cline, 13);
        sViewsWithIds.put(R.id.tv_con, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityUpmainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private ActivityUpmainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[13]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (android.widget.TextView) bindings[9]
            , (androidx.recyclerview.widget.RecyclerView) bindings[11]
            , (androidx.recyclerview.widget.RecyclerView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[5]
            , (com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[10]
            , (com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton) bindings[1]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.upback.setTag(null);
        this.upcesend.setTag(null);
        this.updown.setTag(null);
        this.upsearch.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.onClick == variableId) {
            setOnClick((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setOnClick(@Nullable android.view.View.OnClickListener OnClick) {
        this.mOnClick = OnClick;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.onClick);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener onClick = mOnClick;

        if ((dirtyFlags & 0x3L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.upback.setOnClickListener(onClick);
            this.upcesend.setOnClickListener(onClick);
            this.updown.setOnClickListener(onClick);
            this.upsearch.setOnClickListener(onClick);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): onClick
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}