package com.kth.iotwarehouse;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.kth.iotwarehouse.databinding.ActivityAliInitBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityDownBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityDownMainBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityHomeBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityInfoBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityPeoplemainBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityRealMainBindingImpl;
import com.kth.iotwarehouse.databinding.ActivitySaveBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityUpmainBindingImpl;
import com.kth.iotwarehouse.databinding.ActivityWelcomeBindingImpl;
import com.kth.iotwarehouse.databinding.FragmentAliListBindingImpl;
import com.kth.iotwarehouse.databinding.FragmentDemoBindingImpl;
import com.kth.iotwarehouse.databinding.FragmentHistroyBindingImpl;
import com.kth.iotwarehouse.databinding.FragmentUserBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYALIINIT = 1;

  private static final int LAYOUT_ACTIVITYDOWN = 2;

  private static final int LAYOUT_ACTIVITYDOWNMAIN = 3;

  private static final int LAYOUT_ACTIVITYHOME = 4;

  private static final int LAYOUT_ACTIVITYINFO = 5;

  private static final int LAYOUT_ACTIVITYPEOPLEMAIN = 6;

  private static final int LAYOUT_ACTIVITYREALMAIN = 7;

  private static final int LAYOUT_ACTIVITYSAVE = 8;

  private static final int LAYOUT_ACTIVITYUPMAIN = 9;

  private static final int LAYOUT_ACTIVITYWELCOME = 10;

  private static final int LAYOUT_FRAGMENTALILIST = 11;

  private static final int LAYOUT_FRAGMENTDEMO = 12;

  private static final int LAYOUT_FRAGMENTHISTROY = 13;

  private static final int LAYOUT_FRAGMENTUSER = 14;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(14);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_ali_init, LAYOUT_ACTIVITYALIINIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_down, LAYOUT_ACTIVITYDOWN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_down_main, LAYOUT_ACTIVITYDOWNMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_home, LAYOUT_ACTIVITYHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_info, LAYOUT_ACTIVITYINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_peoplemain, LAYOUT_ACTIVITYPEOPLEMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_real_main, LAYOUT_ACTIVITYREALMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_save, LAYOUT_ACTIVITYSAVE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_upmain, LAYOUT_ACTIVITYUPMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.activity_welcome, LAYOUT_ACTIVITYWELCOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.fragment_ali_list, LAYOUT_FRAGMENTALILIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.fragment_demo, LAYOUT_FRAGMENTDEMO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.fragment_histroy, LAYOUT_FRAGMENTHISTROY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.kth.iotwarehouse.R.layout.fragment_user, LAYOUT_FRAGMENTUSER);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYALIINIT: {
          if ("layout/activity_ali_init_0".equals(tag)) {
            return new ActivityAliInitBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_ali_init is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYDOWN: {
          if ("layout/activity_down_0".equals(tag)) {
            return new ActivityDownBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_down is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYDOWNMAIN: {
          if ("layout/activity_down_main_0".equals(tag)) {
            return new ActivityDownMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_down_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYHOME: {
          if ("layout/activity_home_0".equals(tag)) {
            return new ActivityHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_home is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYINFO: {
          if ("layout/activity_info_0".equals(tag)) {
            return new ActivityInfoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_info is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYPEOPLEMAIN: {
          if ("layout/activity_peoplemain_0".equals(tag)) {
            return new ActivityPeoplemainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_peoplemain is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYREALMAIN: {
          if ("layout/activity_real_main_0".equals(tag)) {
            return new ActivityRealMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_real_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSAVE: {
          if ("layout/activity_save_0".equals(tag)) {
            return new ActivitySaveBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_save is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYUPMAIN: {
          if ("layout/activity_upmain_0".equals(tag)) {
            return new ActivityUpmainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_upmain is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYWELCOME: {
          if ("layout/activity_welcome_0".equals(tag)) {
            return new ActivityWelcomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_welcome is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTALILIST: {
          if ("layout/fragment_ali_list_0".equals(tag)) {
            return new FragmentAliListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_ali_list is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTDEMO: {
          if ("layout/fragment_demo_0".equals(tag)) {
            return new FragmentDemoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_demo is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTHISTROY: {
          if ("layout/fragment_histroy_0".equals(tag)) {
            return new FragmentHistroyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_histroy is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTUSER: {
          if ("layout/fragment_user_0".equals(tag)) {
            return new FragmentUserBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_user is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(3);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    result.add(new cn.bingoogolapple.baseadapter.DataBinderMapperImpl());
    result.add(new cn.bingoogolapple.photopicker.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(6);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "model");
      sKeys.put(2, "onClick");
      sKeys.put(3, "statusModel");
      sKeys.put(4, "uiHandler");
      sKeys.put(5, "viewHolder");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(14);

    static {
      sKeys.put("layout/activity_ali_init_0", com.kth.iotwarehouse.R.layout.activity_ali_init);
      sKeys.put("layout/activity_down_0", com.kth.iotwarehouse.R.layout.activity_down);
      sKeys.put("layout/activity_down_main_0", com.kth.iotwarehouse.R.layout.activity_down_main);
      sKeys.put("layout/activity_home_0", com.kth.iotwarehouse.R.layout.activity_home);
      sKeys.put("layout/activity_info_0", com.kth.iotwarehouse.R.layout.activity_info);
      sKeys.put("layout/activity_peoplemain_0", com.kth.iotwarehouse.R.layout.activity_peoplemain);
      sKeys.put("layout/activity_real_main_0", com.kth.iotwarehouse.R.layout.activity_real_main);
      sKeys.put("layout/activity_save_0", com.kth.iotwarehouse.R.layout.activity_save);
      sKeys.put("layout/activity_upmain_0", com.kth.iotwarehouse.R.layout.activity_upmain);
      sKeys.put("layout/activity_welcome_0", com.kth.iotwarehouse.R.layout.activity_welcome);
      sKeys.put("layout/fragment_ali_list_0", com.kth.iotwarehouse.R.layout.fragment_ali_list);
      sKeys.put("layout/fragment_demo_0", com.kth.iotwarehouse.R.layout.fragment_demo);
      sKeys.put("layout/fragment_histroy_0", com.kth.iotwarehouse.R.layout.fragment_histroy);
      sKeys.put("layout/fragment_user_0", com.kth.iotwarehouse.R.layout.fragment_user);
    }
  }
}
