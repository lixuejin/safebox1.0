package com.sdwm.safebox;

import java.util.List;

public class SafeBoxOne {
//    /dev/ttyS1 /dev/ttyACM0   /dev/ttyACM1   /dev/ttyACM2
    public static String ioport ="/dev/ttyS1";
    public static String scanUpUsb="/dev/ttyUSB0";
    public static String scanSidePortUsb="/dev/ttyUSB1";
    public static String scanUp = "/dev/ttyACM0";
    public static String scanUpTwo = "/dev/ttyACM1";
    public static String scanSide = "/dev/ttyACM2";
    static {
        loadNativeLibrary();
    }

    private static synchronized void loadNativeLibrary() {
        System.loadLibrary("SafeBoxOne");
    }
    public static native int init(String ioPort,String scanUp,String scanUpTwo,String scanSide,String rfid);
    public static native int getStatus();
    public static native int unlocking();
    public static native int upReset();
    public static native List<String> putOnDisk(int[] result,byte[] rfidCode,int[] rfidCount);
    public static native List<String> robotGoOn(int[] result, byte[] rfidCode, int[] rfidCount);
    public static native int unlockingCover();//盖子开锁
    public static native int pushDownDisk();//推下硬盘
    public static native int back();

}
