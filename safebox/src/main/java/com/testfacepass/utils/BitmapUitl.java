package com.testfacepass.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class BitmapUitl {
    //选择角度
    public static Bitmap createPhotos(Bitmap bitmap,int degrees){
        if(bitmap!=null){
            if(degrees == 0){
                return bitmap;
            }
            Matrix m=new Matrix();
            try{
                m.setRotate(degrees, bitmap.getWidth()/2, bitmap.getHeight()/2);//90就是我们需要选择的90度
                Bitmap bmp2=Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                bitmap.recycle();
                bitmap=bmp2;
            }catch(Exception ex){
                System.out.print("创建图片失败！"+ex);
            }
        }
        return bitmap;
    }

}
