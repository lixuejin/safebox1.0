package com.testfacepass;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kth.safebox.R;
import com.megvii.AuthApi.AuthApi;
import com.megvii.AuthApi.AuthCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class AuthActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MEGVII-LICENSE";
    public static final int GET_AUTH_OK = 0;

    /*
    * 使用demo过程中请根据自己的实际情况配置cert以及active路径
    * */
    public static final String CERT_PATH = "/Download/stage.cert.csv";
    public static final String ACTIVE_PATH = "/Download/active.txt.csv";
  //      public static final String CERT_PATH = "/stage.cert";
   // public static final String ACTIVE_PATH = "/active.txt";

    /* 程序所需权限 ：相机 文件存储 网络访问 */
    private static final int PERMISSIONS_REQUEST = 1;
    private static final String PERMISSION_CAMERA = Manifest.permission.CAMERA;
    private static final String PERMISSION_WRITE_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String PERMISSION_READ_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private static final String PERMISSION_INTERNET = Manifest.permission.INTERNET;
    private static final String PERMISSION_ACCESS_NETWORK_STATE = Manifest.permission.ACCESS_NETWORK_STATE;
    private String[] Permission = new String[]{PERMISSION_CAMERA, PERMISSION_WRITE_STORAGE, PERMISSION_READ_STORAGE, PERMISSION_INTERNET, PERMISSION_ACCESS_NETWORK_STATE};
    private TextView mAuthStatus;
    private AuthApi obj;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        mAuthStatus = (TextView) findViewById(R.id.textView_auth_status);

        /* 申请程序所需权限 */
        if (!hasPermission()) {
            requestPermission();
        }
        mAuthStatus.setTextColor(Color.BLUE);
        mAuthStatus.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        mAuthStatus.setBackgroundColor(Color.YELLOW);
        obj = new AuthApi();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_fingerprint:

                try {
                    //此处较3.5.0 版本增加是否扣点检查接口，可先检查申请授权是否扣点后再进行授权
                    checkSingleAuthStatus();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.button_activate:
                try {
                    //此处较3.5.0 版本增加判断是否扣点检查接口，可先检查申请授权是否扣点再进行授权
                    checkDoubleAuthStatus();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.button_proxy:

                //此处较3.5.0 版本增加判断是否扣点检查接口，可先检查申请授权是否扣点再进行授权
                checkSingleAuthStatusViaProxy();

                break;
            case R.id.button_proxy_d:
                try {
                    //此处较3.5.0 版本增加判断是否扣点检查接口，可先检查申请授权是否扣点再进行授权
                    checkDoubleAuthStatusViaProxy();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.button_ok:
                enterMainactivity();
                break;
        }
    }



    /*
       外网模式检查单一授权状态，用来检查授权请求是否扣点
       */

    private void checkSingleAuthStatus() throws IOException {

        String cert = readExternal(CERT_PATH).trim();
        if (TextUtils.isEmpty(cert)) {
            Toast.makeText(this, "cert is null", Toast.LENGTH_SHORT).show();
            return;
        }

        obj.checkAuthStatus(cert, "", new AuthCallback() {
            @Override
            public void onAuthResponse(final int i, final String s) {
//                返回code为1表示授权已存在，需要调用authDevice接口恢复，不额外扣点，
//                返回code为2表示授权不存在或者无法恢复，调用authDevice会额外扣点
//                可根据code 自行处理逻辑
                Log.e(TAG, i + "返回码");
                if (i == 1 || i == 2) {
                    try {
                        //申请授权
                        Log.e(TAG, "开始申请授权");
                        singleCertification();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }

    private void checkDoubleAuthStatus() throws IOException {
        String cert = readExternal(CERT_PATH).trim();
        String active = readExternal(ACTIVE_PATH).trim();
        if (TextUtils.isEmpty(cert)) {
            Toast.makeText(this, "cert is null", Toast.LENGTH_SHORT).show();
            return;
        }
        obj.checkAuthStatus(cert, active, new AuthCallback() {
            @Override
            public void onAuthResponse(final int i, final String s) {
//                返回code为1表示授权已存在，需要调用authDevice接口恢复，不额外扣点，
//                返回code为2表示授权不存在或者无法恢复，调用authDevice会额外扣点
//                可根据code 自行处理逻辑
                Log.e(TAG, i + "返回码");
                if (i == 1 || i == 2) {
                    try {
                        doubleCertification();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    /*
    无网连跳板机模式单一授权检查授权状态
    */
    private void checkSingleAuthStatusViaProxy() {

        obj.checkAuthStatusViaProxy("10.100.15.65:6666", "CBG_Android_Face_Reco---34-Trial-one-stage.cert", "", new AuthCallback() {
            @Override
            public void onAuthResponse(int i, String s) {
                // 返回code为1表示授权已存在，需要调用authDevice接口恢复，不额外扣点，
//                返回code为2表示授权不存在或者无法恢复，调用authDevice会额外扣点
//                可根据code 自行处理逻辑
                Log.e(TAG, i + "返回码");
                if (i == 1 || i == 2) {
                    singleCertificationFromHost();
                }
            }
        });
    }

    /*
无网连跳板机模式双重授权检查授权状态
*/
    private void checkDoubleAuthStatusViaProxy() throws IOException {
        String active = readExternal(ACTIVE_PATH).trim();

        if (TextUtils.isEmpty(active)) {
            Toast.makeText(this, "active is null", Toast.LENGTH_SHORT).show();
            return;
        }
        obj.checkAuthStatusViaProxy("10.100.14.123:6666",
                "CBG_Android_Face_Reco---90-Trial-two-stage.cert", active, new AuthCallback() {
                    @Override
                    public void onAuthResponse(int i, String s) {
                        //返回code为1表示授权已存在，需要调用authDevice接口恢复，不额外扣点，
                        // 返回code为2表示授权不存在或者无法恢复，调用authDevice会额外扣点
                        // 可根据code 自行处理逻辑
                        Log.e(TAG, i + "返回码");
                        if (i == 1 || i == 2) {
                            try {
                                doubleCertificationFromHost();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                });
    }

    private void singleCertification() throws IOException {

        String cert = readExternal(CERT_PATH).trim();

        if (TextUtils.isEmpty(cert)) {
            Toast.makeText(this, "cert is null", Toast.LENGTH_SHORT).show();
            return;
        }

        obj.authDevice(cert, "", new AuthCallback() {
            @Override
            public void onAuthResponse(final int i, final String s) {
                if (i == GET_AUTH_OK) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAuthResult("Apply update: OK");
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAuthResult(i + " " + s);
                        }
                    });
                }
            }
        });

    }

    /**
     * .
     *
     * @throws IOException
     */
    private void doubleCertification() throws IOException {

        String cert = readExternal(CERT_PATH).trim();
        String active = readExternal(ACTIVE_PATH).trim();
        if (TextUtils.isEmpty(cert) || TextUtils.isEmpty(active)) {
            Toast.makeText(this, "cert or active is null", Toast.LENGTH_SHORT).show();
            return;
        }
        obj.authDevice(cert, active, new AuthCallback() {
            @Override
            public void onAuthResponse(final int i, final String s) {
                if (i == GET_AUTH_OK) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAuthResult("Apply update: OK");
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAuthResult(i + "  " + s);
                        }
                    });
                }
            }
        });
    }

    private void singleCertificationFromHost() {

        obj.authDeviceViaProxy("10.156.4.196:6666", "CBG_Android_Face_Reco---90-Trial-two-stage.cert", "", new AuthCallback() {
            @Override
            public void onAuthResponse(final int i, final String s) {

                if (i == GET_AUTH_OK) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAuthResult("Apply update: OK");
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showAuthResult(i + "  " + s);
                        }
                    });

                }
            }
        });
    }

    private void doubleCertificationFromHost() throws IOException {

        String active = readExternal(ACTIVE_PATH).trim();

        if (TextUtils.isEmpty(active)) {
            Toast.makeText(this, "active is null", Toast.LENGTH_SHORT).show();
            return;
        }

        obj.authDeviceViaProxy("10.156.4.196:6666",
                "CBG_Android_Face_Reco---90-Trial-two-stage.cert", active, new AuthCallback() {
                    @Override
                    public void onAuthResponse(final int i, final String s) {

                        if (i == GET_AUTH_OK) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showAuthResult("Apply update: OK");
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showAuthResult(i + "  " + s);
                                }
                            });
                        }
                    }
                });
    }

    public String readExternal(String filename) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            filename = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + filename;
            File file = new File(filename);
            if (!file.exists()) {
                return "";
            }
            FileInputStream inputStream = new FileInputStream(filename);
            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            while (len > 0) {
                sb.append(new String(buffer, 0, len));
                len = inputStream.read(buffer);
            }
            inputStream.close();
        }

        return sb.toString();
    }

    private void showAuthResult(String res) {
        mAuthStatus.setText(res);
    }

    private void enterMainactivity() {
        String strAuth = mAuthStatus.getText().toString();
        SharedPreferences preferences = getSharedPreferences("auth_sp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("auth", true);
        editor.apply();

        if (strAuth.equals("Apply update: OK")) {
            Log.d(TAG, "before enter MainActivity ");
            Intent intent = new Intent(AuthActivity.this, MainActivity.class);
            startActivity(intent);
            AuthActivity.this.finish();
            /* 用户可以记录设备激活状态，在下次启动app时，直接进入主界面 */
            /* 用户可以记录设备激活状态，在下次启动app时，直接进入主界面 */
            /* 用户可以记录设备激活状态，在下次启动app时，直接进入主界面 */
        }
    }

    /* 判断程序是否有所需权限 android22以上需要自申请权限 */
    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(PERMISSION_CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(PERMISSION_READ_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(PERMISSION_WRITE_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(PERMISSION_INTERNET) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(PERMISSION_ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    /* 请求程序所需权限 */
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(Permission, PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST) {
            boolean granted = true;
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED)
                    granted = false;
            }
            if (!granted) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    if (!shouldShowRequestPermissionRationale(PERMISSION_CAMERA)
                            || !shouldShowRequestPermissionRationale(PERMISSION_READ_STORAGE)
                            || !shouldShowRequestPermissionRationale(PERMISSION_WRITE_STORAGE)
                            || !shouldShowRequestPermissionRationale(PERMISSION_INTERNET)
                            || !shouldShowRequestPermissionRationale(PERMISSION_ACCESS_NETWORK_STATE)) {
                        Toast.makeText(getApplicationContext(), "需要开启摄像头网络文件存储权限", Toast.LENGTH_SHORT).show();
                    }
            }
        }
    }


}
