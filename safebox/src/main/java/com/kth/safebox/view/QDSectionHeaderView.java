/*
 * Tencent is pleased to support the open source community by making QMUI_Android available.
 *
 * Copyright (C) 2017-2018 THL A29 Limited, a Tencent company. All rights reserved.
 *
 * Licensed under the MIT License (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.kth.safebox.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;

import com.kth.safebox.model.SectionHeader;
import com.kth.safebox.util.DateUtil;

public class QDSectionHeaderView extends LinearLayout {

    private TextView name;
    private TextView time;
    private ImageView mArrowView;



    public QDSectionHeaderView(Context context) {
        this(context, null);
    }

    public QDSectionHeaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setBackgroundColor(Color.WHITE);
        LayoutInflater.from(context).inflate(R.layout.item_ali_list, this);
        name = findViewById(R.id.item_ali_list_name);
        time = findViewById(R.id.item_ali_list_in_time);
        mArrowView = findViewById(R.id.item_ali_list_img);
//        mArrowView.setImageDrawable(QMUIResHelper.getAttrDrawable(getContext(),
//                R.attr.qmui_common_list_item_chevron));
    }

    public ImageView getArrowView() {
        return mArrowView;
    }

    public void render(SectionHeader header, boolean isFold) {
        name.setText(header.getPutListEntity().getLsitName());
        time.setText(DateUtil.dateToString_s(header.getPutListEntity().getInTime()));
        mArrowView.setRotation(isFold ? 0f : 90f);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
