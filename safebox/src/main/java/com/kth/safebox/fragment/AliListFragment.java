package com.kth.safebox.fragment;

import android.os.Bundle;

import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kth.greendao.gen.DiskEntityDao;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.adapter.QDListSectionAdapter;
import com.kth.safebox.base.BaseFragment;
//import megvii.kth.iotwarehouse.databinding.FragmentAliListBinding;
import com.kth.safebox.model.SectionHeader;
import com.kth.safebox.model.SectionItem;
import com.kth.safebox.sqlist.DiskEntity;
import com.kth.safebox.sqlist.PutListEntity;
import com.kth.safebox.util.DbCore;

import com.kth.safebox.R;
import com.kth.safebox.databinding.FragmentAliListBinding;
import com.qmuiteam.qmui.recyclerView.QMUIRVDraggableScrollBar;
import com.qmuiteam.qmui.widget.section.QMUISection;
import com.qmuiteam.qmui.widget.section.QMUIStickySectionAdapter;

import java.util.ArrayList;
import java.util.List;


public class AliListFragment extends BaseFragment<FragmentAliListBinding> implements View.OnClickListener {
    private RecyclerView.LayoutManager mLayoutManager;
    protected QMUIStickySectionAdapter<SectionHeader, SectionItem, QDListSectionAdapter.ViewHolder> mAdapter;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_ali_list;
    }

    @Override
    protected void onBindView(View view, ViewGroup container, Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        initStickyLayout();
        setData();
        binding.setOnClick(this);
    }

    @Override
    protected void initData() {

    }

    protected void initStickyLayout() {
        mLayoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        binding.sectionLayout.setLayoutManager(mLayoutManager);
        QMUIRVDraggableScrollBar scrollBar = new QMUIRVDraggableScrollBar(0, 0, 0);
        scrollBar.setEnableScrollBarFadeInOut(false);
        scrollBar.attachToStickSectionLayout(binding.sectionLayout);
    }

    private void setData() {
        mAdapter = new QDListSectionAdapter(true);
        mAdapter.setCallback(new QMUIStickySectionAdapter.Callback<SectionHeader, SectionItem>() {
            @Override
            public void loadMore(final QMUISection<SectionHeader, SectionItem> section, final boolean loadMoreBefore) {

            }

            @Override
            public void onItemClick(QMUIStickySectionAdapter.ViewHolder holder, int position) {
//                Toast.makeText(getContext(), "click item " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onItemLongClick(QMUIStickySectionAdapter.ViewHolder holder, int position) {
//                Toast.makeText(getContext(), "long click item " + position, Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        binding.sectionLayout.setAdapter(mAdapter, true);
        ArrayList<QMUISection<SectionHeader, SectionItem>> list = new ArrayList<>();
        //列表
        List<PutListEntity> putListEntities = DbCore.getDaoSession().getPutListEntityDao().loadAll();
        for (int i = 0; i < putListEntities.size(); i++) {
            list.add(createSection(putListEntities.get(i), true));
        }
        mAdapter.setData(list);
    }

    private QMUISection<SectionHeader, SectionItem> createSection(PutListEntity putListEntity, boolean isFold) {
        SectionHeader header = new SectionHeader(putListEntity);
        ArrayList<SectionItem> contents = new ArrayList<>();
        //查找id
        List<Integer> integerList = putListEntity.getDiskList();
        for (int i = 0; i < integerList.size(); i++) {
            DiskEntity diskEntity = DbCore.getDaoSession().getDiskEntityDao().queryBuilder().where(DiskEntityDao.Properties.Id.eq(integerList.get(i).longValue())).unique();
            contents.add(new SectionItem(diskEntity));

        }
        QMUISection<SectionHeader, SectionItem> section = new QMUISection<>(header, contents, isFold);
        // if test load more, you can open the code
        section.setExistAfterDataToLoad(false);
        section.setExistBeforeDataToLoad(false);
        return section;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}