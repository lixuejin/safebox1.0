package com.kth.safebox.fragment;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;
import com.kth.safebox.databinding.FragmentHistroyBinding;

import com.kth.safebox.adapter.PutListAdapter;
import com.kth.safebox.base.BaseFragment;
//import megvii.kth.iotwarehouse.databinding.FragmentAliListBinding;
//import megvii.kth.iotwarehouse.databinding.FragmentHistroyBinding;
import com.kth.safebox.sqlist.DiskEntity;
import com.kth.safebox.util.DbCore;
//import megvii.testfacepass.databinding.FragmentHistroyBinding;

import java.util.ArrayList;
import java.util.List;

public class HistroyFragment extends BaseFragment<FragmentHistroyBinding> implements View.OnClickListener {

    private List<DiskEntity> mData = new ArrayList<>();
    @Override
    public int getLayoutId() {
        return R.layout.fragment_histroy;
    }

    @Override
    protected void onBindView(View view, ViewGroup container, Bundle savedInstanceState) {

    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
    }

    @Override
    protected void initData() {
        initListView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
    private void initListView() {
        mData.clear();
        List<DiskEntity> diskEntities = DbCore.getDaoSession().getDiskEntityDao().loadAll();
        mData.addAll(diskEntities);
        PutListAdapter adapter = new PutListAdapter(getContext(), mData);
        binding.listview.setAdapter(adapter);
    }
}