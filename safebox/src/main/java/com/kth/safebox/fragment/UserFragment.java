package com.kth.safebox.fragment;

import android.os.Bundle;

import android.view.View;
import android.view.ViewGroup;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;
import com.kth.safebox.databinding.FragmentUserBinding;

import com.kth.safebox.adapter.UserInfoAdpter;
import com.kth.safebox.base.BaseFragment;
//import megvii.kth.iotwarehouse.databinding.FragmentUserBinding;
import com.kth.safebox.sqlist.UserLogEntity;
import com.kth.safebox.util.DbCore;
//import megvii.testfacepass.databinding.FragmentUserBinding;

import java.util.ArrayList;
import java.util.List;


public  class UserFragment extends BaseFragment<FragmentUserBinding> implements View.OnClickListener {

    private List<UserLogEntity> mData = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user;
    }

    @Override
    protected void onBindView(View view, ViewGroup container, Bundle savedInstanceState) {
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        initListView();
    }

    @Override
    protected void initData() {

    }
    private void initListView() {
        mData.clear();
        List<UserLogEntity> userLogEntities = DbCore.getDaoSession().getUserLogEntityDao().loadAll();
        mData.addAll(userLogEntities);
        UserInfoAdpter adapter = new UserInfoAdpter(getContext(), mData);
        binding.listview.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
