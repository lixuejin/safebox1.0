package com.kth.safebox.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kth.safebox.base.BaseApplication;
import com.kth.safebox.bean.User;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;
import com.qmuiteam.qmui.skin.QMUISkinManager;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 2017/6/24.
 */

public class UserExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;    //父activity
    protected Resources res;
    private LayoutInflater mChildInflater;    //用于加载分组的布局xml
    private LayoutInflater mGroupInflater;    //用于加载对应分组用户的布局xml
    List<String> groups = new ArrayList<String>();
    List<List<User>> children = new ArrayList<List<User>>();
    private int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;
    public UserExpandableListAdapter(Context c, List<String> groups, List<List<User>> children) {
        mChildInflater = LayoutInflater.from(c);
        mGroupInflater = LayoutInflater.from(c);
        this.groups = groups;
        this.children = children;
        context = c;
        res = c.getResources();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) { //根据组索引和item索引，取得listitem
        // TODO Auto-generated method stub
        return children.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) { //返回item索引
        // TODO Auto-generated method stub
        return childPosition;
    }

    //分组视图
    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        View myView = mChildInflater.inflate(R.layout.children, null);

        if (groups == null || groups.size() == 0 || children == null || children.size() == 0) {
            return myView;
        }
        final User user = children.get(groupPosition).get(childPosition);

        TextView childTv = (TextView) myView.findViewById(R.id.child_name);
        TextView childIp = (TextView) myView.findViewById(R.id.child_ip);
        LinearLayout child_ly  = myView.findViewById(R.id.child_ly);
        childTv.setText(user.getUserName());    //用户名显示
        childIp.setText(user.getIp());    //IP显示
        if (childPosition %2 ==0){
            child_ly.setBackgroundColor(Color.parseColor("#4ca4df"));
        }else {
            child_ly.setBackgroundColor(Color.parseColor("#0977bb"));

        }

        myView.setOnClickListener(new View.OnClickListener() {    //点击子项

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //
                new QMUIDialog.MessageDialogBuilder(context)
                        .setTitle("选择下位机")
                        .setMessage("您确定要选择本机吗？")
                        .setSkinManager(QMUISkinManager.defaultInstance(context))
                        .addAction("取消", new QMUIDialogAction.ActionListener() {
                            @Override
                            public void onClick(QMUIDialog dialog, int index) {
                                dialog.dismiss();
                            }
                        })
                        .addAction(0, "确定", QMUIDialogAction.ACTION_PROP_POSITIVE, new QMUIDialogAction.ActionListener() {
                            @Override
                            public void onClick(QMUIDialog dialog, int index) {
                                dialog.dismiss();
                                BaseApplication.receiverIp = user.getIp();
                            }
                        })
                        .create(mCurrentDialogStyle).show();


            }

        });

        return myView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {//根据组索引返回分组的子item数
        // TODO Auto-generated method stub
        return children.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) { //根据组索引返回组
        // TODO Auto-generated method stub
        return children.get(groupPosition);
    }

    @Override
    public int getGroupCount() { //返回分组数
        // TODO Auto-generated method stub
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) { //返回分组索引
        // TODO Auto-generated method stub
        return groupPosition;
    }

    //组视图
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View myView = mGroupInflater.inflate(R.layout.groups, null);

        if (groups == null || groups.size() == 0 || children == null || children.size() == 0) {
            return myView;
        }

        //一级菜单收放状态对应图标设置
        ImageView groupImg = (ImageView) myView.findViewById(R.id.group_img);
        if (isExpanded)
            groupImg.setImageDrawable(res.getDrawable(R.drawable.group_exp));
        else
            groupImg.setImageDrawable(res.getDrawable(R.drawable.group_notexp));

        //设置文本内容
        TextView groupTv = (TextView) myView.findViewById(R.id.group);
        groupTv.setText(groups.get(groupPosition));
        TextView groupOnLine = (TextView) myView.findViewById(R.id.group_onlinenum);
        groupOnLine.setText("[" + getChildrenCount(groupPosition) + "]");

        return myView;
    }

    @Override
    public boolean hasStableIds() { //行是否具有唯一id
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) { //行是否可选
        // TODO Auto-generated method stub
        return false;
    }

}
