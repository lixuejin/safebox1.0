package com.kth.safebox.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;

import com.kth.safebox.sqlist.UserLogEntity;
import com.kth.safebox.util.DateUtil;

import java.util.List;

public class UserInfoAdpter extends BaseAdapter {

    private List<UserLogEntity> data;
    private LayoutInflater layoutInflater;
    private Context context;

    public UserInfoAdpter(Context context, List<UserLogEntity> data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    /**
     * 组件集合，对应list.xml中的控件
     *
     * @author Administrator
     */
    public final class ViewHolder {
        public TextView name;
        public TextView time;
        public TextView action;
        public ConstraintLayout linearLayout;
//        public TextView view;
//        public TextView info;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    /**
     * 获得某一位置的数据
     */
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    /**
     * 获得唯一标识
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            //获得组件，实例化组件
            convertView = layoutInflater.inflate(R.layout.item_user_list, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.item_list_put_name);
            viewHolder.time = (TextView) convertView.findViewById(R.id.item_list_put_time);
            viewHolder.action = (TextView) convertView.findViewById(R.id.item_list_action);
            viewHolder.linearLayout = (ConstraintLayout) convertView.findViewById(R.id.item_list_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //绑定数据
        viewHolder.name.setText(data.get(position).getUserId());
        if (data.get(position).getTime() == null) {
            viewHolder.time.setText("存入时间");
        } else {
            viewHolder.time.setText(DateUtil.dateToString_s(data.get(position).getTime()));
        }
        if (data.get(position).getMsgType() == 1) {
            viewHolder.action.setText("存入硬盘");
        }
        if (data.get(position).getMsgType() == 2) {
            viewHolder.action.setText("弹出柜体");
        }
        if (data.get(position).getMsgType() == 3) {
            viewHolder.action.setText("人脸核验");
        }
        if (position % 2 == 0) {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#4ca4df"));

        } else {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#0977bb"));

        }
        return convertView;
    }
}
