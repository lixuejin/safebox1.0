package com.kth.safebox.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;

import com.kth.safebox.bean.WifiBean;

import java.util.List;

public class WifiNameAdpter extends BaseAdapter {

    private List<WifiBean> data;
    private LayoutInflater layoutInflater;
    private Context context;
    private onItemClickListener onItemClickListener;
    public void WifiNameAdpter(WifiNameAdpter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public WifiNameAdpter(Context context, List<WifiBean> data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    /**
     * 组件集合，对应list.xml中的控件
     *
     * @author Administrator
     */
    public final class ViewHolder {
        public TextView name;

        public ConstraintLayout linearLayout;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    /**
     * 获得某一位置的数据
     */
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    /**
     * 获得唯一标识
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            //获得组件，实例化组件
            convertView = layoutInflater.inflate(R.layout.item_wifi_list, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.item_wifi_name);
            viewHolder.linearLayout = (ConstraintLayout) convertView.findViewById(R.id.item_wifi_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //绑定数据
        viewHolder.name.setText(data.get(position).getWifiName());
        if (position % 2 == 0) {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#4ca4df"));
        } else {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#0977bb"));
        }
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClick(view,position,data.get(position));
            }
        });
        return convertView;
    }
    public interface onItemClickListener{
        void onItemClick(View view, int postion, WifiBean wifiBean);
    }
}
