package com.kth.safebox.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.kongzue.dialog.v3.MessageDialog;
import com.kth.safebox.R;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.sqlist.DiskEntity;
import com.kth.safebox.util.DateUtil;

import java.util.List;

public class PutListAdapter extends BaseAdapter {

    private List<DiskEntity> data;
    private LayoutInflater layoutInflater;
    private Context context;

    public PutListAdapter(Context context, List<DiskEntity> data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    /**
     * 组件集合，对应list.xml中的控件
     *
     * @author Administrator
     */
    public final class ViewHolder {
        public TextView name;
        public TextView time;
        public CheckBox isList;
        public ConstraintLayout linearLayout;
//        public TextView view;
//        public TextView info;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    /**
     * 获得某一位置的数据
     */
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    /**
     * 获得唯一标识
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            //获得组件，实例化组件
            convertView = layoutInflater.inflate(R.layout.item_put_list, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.item_list_put_name);
            viewHolder.time = (TextView) convertView.findViewById(R.id.item_list_put_time);
            viewHolder.isList = (CheckBox) convertView.findViewById(R.id.item_list_is_list);
            viewHolder.linearLayout = (ConstraintLayout) convertView.findViewById(R.id.item_list_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //绑定数据
        viewHolder.name.setText(data.get(position).getIdNumber());
        if (data.get(position).getInTime() == null) {
            viewHolder.time.setText("");
        } else {
            viewHolder.time.setText(DateUtil.dateToString_s(data.get(position).getInTime()));
        }
        if (data.get(position).getFromALi()) {
            viewHolder.isList.setChecked(true);

        } else {
            viewHolder.isList.setChecked(false);

        }
        if (position % 2 == 0) {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#4ca4df"));

        } else {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#0977bb"));

        }
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.show((AppCompatActivity) context, "完整编号", data.get(position).getIdNumber(), "确定");
            }
        });
        return convertView;
    }

}
