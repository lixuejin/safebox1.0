package com.kth.safebox.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.model.SectionHeader;
import com.kth.safebox.model.SectionItem;
import com.kth.safebox.util.DateUtil;
import com.kth.safebox.view.QDLoadingItemView;
import com.kth.safebox.view.QDSectionHeaderView;

import com.kth.safebox.R;
import com.qmuiteam.qmui.widget.section.QMUISection;
import com.qmuiteam.qmui.widget.section.QMUIStickySectionAdapter;

public class QDListSectionAdapter extends QMUIStickySectionAdapter<SectionHeader, SectionItem, QDListSectionAdapter.ViewHolder> {

    public final class ViewHolder extends QMUIStickySectionAdapter.ViewHolder {
        public TextView name;
        public TextView time;
        public CheckBox isList;
        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_list_put_name);
            time = itemView.findViewById(R.id.item_list_put_time);
            isList = itemView.findViewById(R.id.item_list_is_list);
        }

    }
    public QDListSectionAdapter(boolean removeSectionTitleIfOnlyOneSection) {
        super(removeSectionTitleIfOnlyOneSection);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateSectionHeaderViewHolder(@NonNull ViewGroup viewGroup) {
        return new ViewHolder(new QDSectionHeaderView(viewGroup.getContext()));
    }

    @NonNull
    @Override
    protected ViewHolder onCreateSectionItemViewHolder(@NonNull ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_put_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateSectionLoadingViewHolder(@NonNull ViewGroup viewGroup) {
        return new ViewHolder(new QDLoadingItemView(viewGroup.getContext()));
    }

    @NonNull
    @Override
    protected ViewHolder onCreateCustomItemViewHolder(@NonNull ViewGroup viewGroup, int type) {
        return null;
    }

    @Override
    protected void onBindSectionHeader(final ViewHolder holder, final int position, QMUISection<SectionHeader, SectionItem> section) {
        QDSectionHeaderView itemView = (QDSectionHeaderView) holder.itemView;
        itemView.render(section.getHeader(), section.isFold());
        itemView.getArrowView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = holder.isForStickyHeader ? position : holder.getAdapterPosition();
                toggleFold(pos, false);
            }
        });
    }

    @Override
    protected void onBindSectionItem(ViewHolder holder, int position, QMUISection<SectionHeader, SectionItem> section, int itemIndex) {
         holder.name.setText(section.getItemAt(itemIndex).getDiskEntity().getIdNumber());
        holder.time.setText(DateUtil.dateToString_s(section.getItemAt(itemIndex).getDiskEntity().getInTime()));
        if(section.getItemAt(itemIndex).getDiskEntity().getFromALi()){
            holder.isList.setChecked(true);
        }else {
            holder.isList.setChecked(false);
        }

    }

}

