package com.kth.safebox.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.R;

import com.kth.safebox.sqlist.UserEntity;

import java.util.List;

public class UserListAdapter extends ArrayAdapter<UserEntity> {

    private List<UserEntity> data;

    private int resourceId;

    // 适配器的构造函数，把要适配的数据传入这里
    public UserListAdapter(Context context, int textViewResourceId, List<UserEntity> objects){
        super(context,textViewResourceId,objects);
        resourceId=textViewResourceId;
        data = objects;
    }
    /**
     * 组件集合，对应list.xml中的控件
     *
     * @author Administrator
     */
    public final class ViewHolder {
        public TextView name;
        public CheckBox checkBox;
        public ConstraintLayout linearLayout;

    }
    public void remove(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return data.size();
    }

    /**
     * 获得某一位置的数据
     * @return
     */
    @Override
    public UserEntity getItem(int position) {
        return data.get(position);
    }

    /**
     * 获得唯一标识
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            //获得组件，实例化组件
            convertView = LayoutInflater.from(getContext()).inflate(resourceId,parent,false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.item_user_name);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.item_user_check);
            viewHolder.linearLayout = (ConstraintLayout) convertView.findViewById(R.id.item_user_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //绑定数据
        viewHolder.name.setText(data.get(position).getName());
        if (position % 2 == 0) {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#4ca4df"));

        } else {
            viewHolder.linearLayout.setBackgroundColor(Color.parseColor("#0977bb"));

        }
        return convertView;
    }
}
