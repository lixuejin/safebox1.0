package com.kth.safebox.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.interfaces.OnInputDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.InputDialog;
import com.kongzue.dialog.v3.MessageDialog;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.adapter.WifiHaveAdpter;
import com.kth.safebox.adapter.WifiNameAdpter;
import com.kth.safebox.base.BaseActivity;
import com.kth.safebox.base.Config;
import com.kth.safebox.bean.WifiBean;
//import megvii.kth.iotwarehouse.databinding.ActivityRealMainBinding;
import com.kth.safebox.sqlist.WifiNameEntity;
import com.kth.safebox.util.DbCore;
import com.kth.safebox.util.LinearGradientFontSpan;
import com.kth.safebox.util.NetWorkUtil;
import com.kth.safebox.util.WifiHelper;
//import megvii.testfacepass.databinding.ActivityRealMainBinding;

import com.kth.safebox.R;
import com.kth.safebox.databinding.ActivityRealMainBinding;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

public class SetWifiActivity extends BaseActivity<ActivityRealMainBinding> implements OnClickListener {

    private List<WifiBean> allWifiData = new ArrayList<>();//所有的WiFi
    private List<WifiNameEntity> haveWifiData = new ArrayList<>();//保存的WiFi
    private WifiBroadcastReceiver wifiReceiver;
    private WifiHelper mWifiHelper;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_real_main;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        binding.tvHomeTitle.setText(LinearGradientFontSpan.getRadiusGradientSpan(getString(R.string.title), Color.parseColor("#009df4"), Color.parseColor("#00e9d0")));
    }

    @Override
    protected void initData() {
        mWifiHelper = new WifiHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //注册广播
        wifiReceiver = new WifiBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);//监听wifi连接状态广播,是否连接了一个有效路由
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);//监听wifi列表变化（开启一个热点或者关闭一个热点）
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        this.registerReceiver(wifiReceiver, filter);
        mWifiHelper.startScan();//扫描
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(wifiReceiver);

    }


    /**
     * 监听wifi状态
     * 当都没有可用连接，系统会连接已经保存的网路
     */
    public class WifiBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {//wifi连接网络状态变化
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (NetworkInfo.State.DISCONNECTED == info.getState()) {//wifi断开连接
                    binding.updevice.setText("无连接");
                    binding.updevice.setTextColor(Color.RED);
                    //判断是否有网络
                    if (NetWorkUtil.isNetworkConnected(context)) {
                        binding.updevice.setText("已连接:4G");
                        binding.updevice.setTextColor(Color.GREEN);
                    }
                } else if (NetworkInfo.State.CONNECTED == info.getState()) {//wifi连接上了
                    WifiHelper wifiHelper = new WifiHelper(context);
                    binding.updevice.setText(wifiHelper.getCurrentWifiSSID());
                    binding.updevice.setTextColor(Color.GREEN);
                    //测试得开线程
//                    Logger.d(WifiUtil.ping());
                } else if (NetworkInfo.State.CONNECTING == info.getState()) {//正在连接
                    binding.updevice.setText("正在连接");
                    binding.updevice.setTextColor(Color.YELLOW);
                }
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {  //扫描结果
//                Log.i(TAG, "网络列表变化了");//几秒回调一次
                wifiListChange();
            } else if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {//密码错误
                int linkWifiResult = intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, 123);
                if (linkWifiResult == WifiManager.ERROR_AUTHENTICATING) {
                    Toast.makeText(context, "密码错误", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    /**
     * wifi列表改变
     * 获取wifi列表然后将bean转成自己定义的WifiBean
     */
    public void wifiListChange() {
        //改变已连接的WiFi
        haveWifiData.clear();
        List<WifiNameEntity> wifiNameEntities = DbCore.getDaoSession().getWifiNameEntityDao().loadAll();
        haveWifiData.addAll(wifiNameEntities);
        WifiHaveAdpter haveAdapter = new WifiHaveAdpter(context, haveWifiData);
        binding.listIsPairing.setAdapter(haveAdapter);
        haveAdapter.WifiHaveAdpter(new WifiHaveAdpter.onItemClickListener() {
            @Override
            public void onItemClick(View view, int postion, WifiNameEntity wifiNameEntity) {
                MessageDialog.show((AppCompatActivity) context, wifiNameEntity.getSsid(), "请选择连接或删除", "连接", "删除")
                        .setOnOkButtonClickListener(new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                mWifiHelper.connectWifi(wifiNameEntity.getSsid(), wifiNameEntity.getPassword(), Config.passwordType);
                                return false;
                            }
                        })
                        .setOnCancelButtonClickListener(new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                mWifiHelper.removeWifiBySsid(wifiNameEntity.getSsid());
                                DbCore.getDaoSession().getWifiNameEntityDao().delete(wifiNameEntity);
                                wifiListChange();
                                return false;
                            }
                        });
            }
        });
        //改变搜索到的WiFi
        allWifiData.clear();
        List<ScanResult> scanResults = mWifiHelper.getFilterScanResult();
        for (int i = 0; i < scanResults.size(); i++) {

            ScanResult scanResult = scanResults.get(i);
            WifiBean wifiBean = new WifiBean(scanResult.SSID, String.valueOf(mWifiHelper.getLevel(scanResult.level))
                    , "", scanResult.capabilities, scanResult);
            allWifiData.add(wifiBean);
        }
        WifiNameAdpter allAdapter = new WifiNameAdpter(context, allWifiData);
        allAdapter.WifiNameAdpter(new WifiNameAdpter.onItemClickListener() {
            @Override
            public void onItemClick(View view, int postion, WifiBean wifiBean) {
                add(wifiBean);
            }
        });
        binding.listHavePairing.setAdapter(allAdapter);
    }

    private void add(WifiBean wifiBean) {
        String ssid = wifiBean.getWifiName();
        InputDialog.show((AppCompatActivity) context, wifiBean.getWifiName(), "在此输入密码", "确定", "取消")
                .setOnOkButtonClickListener(new OnInputDialogButtonClickListener() {
                    @Override
                    public boolean onClick(BaseDialog baseDialog, View v, String inputStr) {
                        //inputStr 即当前输入的文本
                        String password = inputStr;
                        WifiNameEntity wifiNameEntity = new WifiNameEntity(null, ssid, password);

                        //保存本地
                        List<WifiNameEntity> wifiNameEntities = DbCore.getDaoSession().getWifiNameEntityDao().loadAll();
                        if (wifiNameEntities.size() == 0) {
                            //没有则存数据库
                            DbCore.getDaoSession().getWifiNameEntityDao().insert(wifiNameEntity);
                            Logger.d("存");
                        } else {
                            //有则更新
                            wifiNameEntities.get(0).setSsid(ssid);
                            wifiNameEntities.get(0).setPassword(password);
                            DbCore.getDaoSession().getWifiNameEntityDao().update(wifiNameEntities.get(0));
                            Logger.d("更新");
                        }
                        //连接
                        mWifiHelper.connectWifi(ssid, password, Config.passwordType);
                        wifiListChange();
                        return false;
                    }
                });
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.break_home:
                goActivity(HomeActivity.class);
                break;
            case R.id.refresh:
                mWifiHelper.startScan();//扫描
                break;
        }
    }


}
