package com.kth.safebox.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linksdk.tmp.api.OutputParams;
import com.aliyun.alink.linksdk.tmp.device.payload.ValueWrapper;
import com.aliyun.alink.linksdk.tmp.listener.IPublishResourceListener;
import com.aliyun.alink.linksdk.tools.AError;
import com.kongzue.dialog.v3.MessageDialog;
import com.kth.greendao.gen.DiskEntityDao;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.adapter.PutListAdapter;
import com.kth.safebox.base.BaseActivity;
import com.kth.safebox.base.BaseApplication;
import com.kth.safebox.base.Config;
import com.kth.safebox.bean.GetBomBean;
//import megvii.kth.iotwarehouse.databinding.ActivitySaveBinding;
import com.kth.safebox.sqlist.DiskEntity;
import com.kth.safebox.sqlist.PutListEntity;
import com.kth.safebox.sqlist.UserLogEntity;
import com.kth.safebox.util.BoxSoUtil;
import com.kth.safebox.util.DbCore;
import com.kth.safebox.util.JSONUtils;
import com.kth.safebox.util.LinearGradientFontSpan;
import com.kth.safebox.util.NetWorkUtil;
import com.kth.safebox.util.WifiHelper;
//import megvii.testfacepass.databinding.ActivitySaveBinding;

import com.kth.safebox.R;
import com.kth.safebox.databinding.ActivitySaveBinding;
import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.widget.QMUIAnimationListView;
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SaveActivity extends BaseActivity<ActivitySaveBinding> implements View.OnClickListener {
    private List<DiskEntity> mData = new ArrayList<>();
    private List<DiskEntity> aLiData = new ArrayList<>();
    private DiskEntity diskEntity = new DiskEntity();
    private Handler handler;
    private List<Integer> DiskList = new ArrayList<>();
    private Boolean outTime = false;
    private QMUITipDialog tipDialog;
    private String diskCard;//扫码的硬盘编号
    private BroadcastReceiver mReceiver;
    private WifiBroadcastReceiver wifiReceiver;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_save;
    }

    @Override
    protected void initView() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);// ���ֳ�������Ļ��״̬
        binding.tvSaveTitle.setText(LinearGradientFontSpan.getRadiusGradientSpan(getString(R.string.title), Color.parseColor("#009df4"), Color.parseColor("#00e9d0")));
        binding.setOnClick(this);
        if (Config.saveTpye == 0) {
            binding.cbSaveIsList.setChecked(true);
            binding.cbSaveNoList.setChecked(false);
            tipDialog = new QMUITipDialog.Builder(context)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_INFO)
                    .setTipWord(("正在下拉BOM，请稍等"))
                    .create();
            tipDialog.show();
            DialogClean(tipDialog, 10000);
        } else if (Config.saveTpye == 1) {
            binding.cbSaveIsList.setChecked(false);
            binding.cbSaveNoList.setChecked(true);
        }
    }

    @Override
    protected void initData() {
        wifiReceiver = new WifiBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);//监听wifi连接状态广播,是否连接了一个有效路由
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        this.registerReceiver(wifiReceiver, filter);

        initListView();
        initHandler();
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Logger.d("接收到的数据是" + Config.data);
                UseEvent(Config.data);
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        registerReceiver(mReceiver, intentFilter);
    }
    private void UseEvent(String data) {
        switch (Config.event) {
            case "thing.service.BomDown":
                GetBomBean getBomBean = JSONUtils.fromJson(data, GetBomBean.class);
                Logger.d("接收到的数据是" + getBomBean.getParams().getBom().get(0).getSn());
                aLiData.clear();
                for (int i = 0; i < getBomBean.getParams().getBom().size(); i++) {
                    DiskEntity diskEntity = new DiskEntity(null, getBomBean.getParams().getBom().get(i).getSn(),
                            getBomBean.getParams().getBom().get(i).getTicketNo(), null, "", "", false);
                    aLiData.add(diskEntity);
                }
                PutListAdapter adapter = new PutListAdapter(context, aLiData);
                binding.listview.setAdapter(adapter);
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    
    private void btnAble(Boolean bool) {
        if (bool) {
            binding.btnSaveOut.setEnabled(true);
            binding.btnSaveSave.setEnabled(true);

//            binding.btnSaveOut.setVisibility(View.VISIBLE);
//            binding.btnSaveSave.setVisibility(View.VISIBLE);
        } else {
            binding.btnSaveOut.setEnabled(false);
            binding.btnSaveSave.setEnabled(false);
//            binding.btnSaveOut.setVisibility(View.INVISIBLE);
//            binding.btnSaveSave.setVisibility(View.INVISIBLE);
        }


    }

    @SuppressLint("HandlerLeak")
    private void initHandler() {
        handler = new Handler() {
            public void handleMessage(Message msg) {
                btnAble(true);
                switch (msg.what) {
                    case 0:
                        MessageDialog.show((AppCompatActivity) context, "警告", (String) msg.obj, "确定");

                        break;
                    case 1:
                        tipDialog = new QMUITipDialog.Builder(context)
                                .setIconType(QMUITipDialog.Builder.ICON_TYPE_INFO)
                                .setTipWord((String) msg.obj)
                                .create();
                        tipDialog.show();
                        DialogClean(tipDialog, 5000);
                        break;
                    case 4:
                        if (Config.saveTpye == 0) {
                            saveInAli();
                        } else if (Config.saveTpye == 1) {
                            saveNormol();
                        }
                        break;
                }

            }
        };
    }
    private void saveNormol() {
        binding.listview.manipulate(new QMUIAnimationListView.Manipulator<PutListAdapter>() {
            @Override
            public void manipulate(PutListAdapter adapter) {
                Date date = new Date(System.currentTimeMillis());
                DiskEntity diskEntity = new DiskEntity(null, diskCard, "", date, BaseApplication.macUp, BaseApplication.macDown, false);
                mData.add(0, diskEntity);
                DbCore.getDaoSession().getDiskEntityDao().insert(diskEntity);
                //搜索data
                DiskEntity disk = DbCore.getDaoSession().getDiskEntityDao().queryBuilder().where(DiskEntityDao.Properties.InTime.eq(date)).unique();
                DiskList.add(disk.getId().intValue());
            }
        });
        //判断是否连续存入
        if (binding.cbSaveContinuousSave.isChecked()) {
            save();
        }
    }

    private void saveInAli() {
        //先移除
        aLiData.remove(diskEntity);
        Date date = new Date(System.currentTimeMillis());
        diskEntity.setInTime(date);
        diskEntity.setUpperMac(BaseApplication.macUp);
        diskEntity.setUnderMac(BaseApplication.macDown);
        diskEntity.setFromALi(true);
        DbCore.getDaoSession().getDiskEntityDao().insert(diskEntity);
        //搜索data，添加进数据库组
        DiskEntity disk = DbCore.getDaoSession().getDiskEntityDao().queryBuilder().where(DiskEntityDao.Properties.InTime.eq(date)).unique();
        HddSnCheckOk(disk);
        DiskList.add(disk.getId().intValue());
        //再加回来，刷新列表
        aLiData.add(diskEntity);
        PutListAdapter adapter = new PutListAdapter(context, aLiData);
        binding.listview.setAdapter(adapter);
        //判断是否连续存入
        if (binding.cbSaveContinuousSave.isChecked()) {
            save();
        }
    }
    private void HddSnCheckOk(DiskEntity diskEntity) {
        // 设备上报
        HashMap<String, ValueWrapper> hashMap = new HashMap<>();
        // TODO 用户根据实际情况设置
        hashMap.put("sn", new ValueWrapper.StringValueWrapper(diskEntity.getIdNumber()));
        hashMap.put("type", new ValueWrapper.IntValueWrapper(0));
        hashMap.put("BoxSn", new ValueWrapper.StringValueWrapper(Config.DeviceName));
        hashMap.put("ticketNo", new ValueWrapper.StringValueWrapper(diskEntity.getTicketNo()));
        OutputParams params = new OutputParams(hashMap);
        LinkKit.getInstance().getDeviceThing().thingEventPost("HddSnCheckOk", params, new IPublishResourceListener() {
            @Override
            public void onSuccess(String resId, Object o) { // 事件上报成功
                Logger.d("成功" + JSON.toJSON(o));
            }

            @Override
            public void onError(String resId, AError aError) { // 事件上报失败
                Logger.d("失败");
                MessageDialog.show((AppCompatActivity) context, "提示", "事件上报失败", "确定");
            }
        });
    }

    private void DialogClean(QMUITipDialog tipDialog, int time) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                tipDialog.dismiss();
            }
        }, time);
    }

    private void initListView() {
        PutListAdapter adapter = new PutListAdapter(context, mData);
        binding.listview.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_save:
                save();
                break;
            case R.id.btn_save_out:
                Date date = new Date(System.currentTimeMillis());
                if (DiskList.size() > 0) {
                    String listName = "";
                    if(Config.saveTpye == 0){
                        listName = "清单存储";
                    }else if(Config.saveTpye == 1){
                        listName = "自由存储";
                    }
                    PutListEntity putListEntity = new PutListEntity(null, listName, DiskList, date,
                            "", BaseApplication.macUp, "", BaseApplication.macDown, false);
                    DbCore.getDaoSession().getPutListEntityDao().insert(putListEntity);
                    UserLogEntity userLogEntity = new UserLogEntity(null, BaseApplication.userEntities.getName(), 1
                            , date, BaseApplication.macUp, BaseApplication.macDown);
                    DbCore.getDaoSession().getUserLogEntityDao().insert(userLogEntity);
                }
                upReset();
                goActivity(HomeActivity.class);
                break;
            case R.id.cb_save_is_list:
                break;
            case R.id.cb_save_no_list:
                break;
            case R.id.cb_save_continuous_save:
                if (binding.cbSaveContinuousSave.isChecked()) {
                    binding.cbSaveContinuousSave.setChecked(true);
                    return;
                }
                if (!binding.cbSaveContinuousSave.isChecked()) {
                    binding.cbSaveContinuousSave.setChecked(false);
                    return;
                }
                break;
            case R.id.tv_box_name:

                break;
        }
    }

    //上位机复位
    private void upReset() {
        BoxSoUtil.instance().upReset(new BoxSoUtil.BoxCallBack() {
            @Override
            public void onTimeOut() {

            }

            @Override
            public void onSuccess(int ret) {

            }

            @Override
            public void onError(String ret) {
                Message msg = new Message();
                msg.what = 0;
                msg.obj = ret;
                handler.sendMessage(msg);
            }
        });
    }

    //存盘开始
    private void save() {
        Message message = new Message();
        message.what = 1;
        message.obj = "放入硬盘后请按两个开始键";
        handler.sendMessage(message);
        BoxSoUtil.instance().putOnDisk(new BoxSoUtil.BoxScanCallBack() {
            @Override
            public void onTimeOut() {

            }

            @Override
            public void onSuccess(List<String> list, byte[] rfidCode) {
                diskCard = "";
                if (Config.saveTpye == 0) {
                    for (int i = 0; i < list.size(); i++) {
                        for (int j = 0; j < aLiData.size(); j++) {
                            if (list.get(i).equals(aLiData.get(j).getIdNumber())) {
                                //找到这个SN号，找到这个类
                                diskCard = list.get(i);
                                diskEntity = aLiData.get(j);
                                break;
                            }
                        }
                    }
                    if (diskCard.equals("")){
                        backDisk();
                        MessageDialog.show((AppCompatActivity) context, "提示", "清单中不包含此硬盘", "确定");
                        return;
                    }
                    pushDownDisk();
                } else if (Config.saveTpye == 1) {
                    for (int i = 0; i < list.size(); i++) {
                        diskCard = diskCard + list.get(i)+"\n";
                    }
                    pushDownDisk();
                }
            }

            @Override
            public void onError(String ret) {
                Message msg = new Message();
                msg.what = 0;
                msg.obj = ret;
                handler.sendMessage(msg);

            }
        });
    }
    //返回硬盘
    private void backDisk(){
        BoxSoUtil.instance().back(new BoxSoUtil.BoxCallBack() {
            @Override
            public void onTimeOut() {

            }

            @Override
            public void onSuccess(int ret) {

            }

            @Override
            public void onError(String ret) {

            }
        });
    }
    //推下去
    private void pushDownDisk() {
        BoxSoUtil.instance().pushDownDisk(new BoxSoUtil.BoxCallBack() {
            @Override
            public void onTimeOut() {

            }

            @Override
            public void onSuccess(int ret) {
                Message msg = new Message();
                msg.what = 4;
                handler.sendMessage(msg);
            }

            @Override
            public void onError(String ret) {
                Message msg = new Message();
                msg.what = 0;
                msg.obj = ret;
                handler.sendMessage(msg);
            }
        });

    }

    /**
     * 监听wifi状态
     * 当都没有可用连接，系统会连接已经保存的网路
     */
    public class WifiBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {//wifi连接网络状态变化
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (NetworkInfo.State.DISCONNECTED == info.getState()) {//wifi断开连接
                    binding.tvBoxName.setText("未连接");
                    binding.tvBoxName.setTextColor(Color.RED);
                    binding.imgBoxLed.setImageResource(R.drawable.img_led_red);
                    //判断是否有网络
                    if (NetWorkUtil.isNetworkConnected(context)){
                        binding.tvBoxName.setText("已连接:4G");
                        binding.tvBoxName.setTextColor(Color.GREEN);
                        binding.imgBoxLed.setImageResource(R.drawable.img_led_green);
                    }
                } else if (NetworkInfo.State.CONNECTED == info.getState()) {//wifi连接上了
                    WifiHelper wifiHelper = new WifiHelper(context);
                    binding.tvBoxName.setText("已连接:" + wifiHelper.getCurrentWifiSSID());
                    binding.tvBoxName.setTextColor(Color.GREEN);
                    binding.imgBoxLed.setImageResource(R.drawable.img_led_green);
                } else if (NetworkInfo.State.CONNECTING == info.getState()) {//正在连接
                    binding.tvBoxName.setText("正在连接");
                    binding.tvBoxName.setTextColor(Color.YELLOW);
                    binding.imgBoxLed.setImageResource(R.drawable.img_led_red);
                }
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {  //扫描结果
//                Log.i(TAG, "网络列表变化了");//几秒回调一次
            } else if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {//密码错误

            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}