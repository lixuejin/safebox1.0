package com.kth.safebox.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linksdk.tmp.api.OutputParams;
import com.aliyun.alink.linksdk.tmp.device.payload.ValueWrapper;
import com.aliyun.alink.linksdk.tmp.listener.IPublishResourceListener;
import com.aliyun.alink.linksdk.tools.AError;
import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.interfaces.OnInputDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.InputDialog;
import com.kongzue.dialog.v3.MessageDialog;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.base.BaseActivity;
import com.kth.safebox.base.BaseApplication;
import com.kth.safebox.base.Config;
//import megvii.kth.iotwarehouse.databinding.ActivityHomeBinding;
import com.kth.safebox.sqlist.UserLogEntity;
import com.kth.safebox.util.BackFace;
import com.kth.safebox.util.BoxSoUtil;
import com.kth.safebox.util.DbCore;
import com.kth.safebox.util.LinearGradientFontSpan;
import com.kth.safebox.util.NetWorkUtil;
import com.kth.safebox.util.WifiHelper;
//import megvii.testfacepass.databinding.ActivityHomeBinding;

import com.kth.safebox.R;
import com.kth.safebox.databinding.ActivityHomeBinding;
import com.orhanobut.logger.Logger;

import java.util.Date;
import java.util.HashMap;

public class HomeActivity extends BaseActivity<ActivityHomeBinding> implements View.OnClickListener {
    private BackFace backFace;//如果没操作，X秒后返回首页的计时器
    private Handler handler;
    private int outbox = 0;
    private BroadcastReceiver mReceiver;
    private WifiBroadcastReceiver wifiReceiver;
    @Override
    protected int getContentViewId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        binding.tvHomeTitle.setText(LinearGradientFontSpan.getRadiusGradientSpan(getString(R.string.title), Color.parseColor("#009df4"), Color.parseColor("#00e9d0")));
        binding.tvHomeUser.setText("当前用户：" + BaseApplication.userEntities.getName());
    }

    @Override
    protected void initData() {
        wifiReceiver = new WifiBroadcastReceiver();
        initHandler();
        backFace = new BackFace(1000 * 60 * 1, 1000, this);

        BoxSoUtil.instance().initUp(new BoxSoUtil.BoxCallBack() {
            @Override
            public void onTimeOut() {

            }

            @Override
            public void onSuccess(int ret) {

            }
            @Override
            public void onError(String ret) {

            }
        });
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Logger.d("接收到的数据是" + Config.data);

            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        wifiReceiver = new WifiBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);//监听wifi连接状态广播,是否连接了一个有效路由
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        this.registerReceiver(wifiReceiver, filter);
        timeStart();
    }

    //region 无操作 返回主页
    private void timeStart() {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                backFace.start();
            }
        });
    }



    /**
     * 主要的方法，重写dispatchTouchEvent
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            //获取触摸动作，如果ACTION_UP，计时开始。
            case MotionEvent.ACTION_UP:
                backFace.start();
                break;
            //否则其他动作计时取消
            default:
                backFace.cancel();
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        super.onPause();
        backFace.cancel();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home_save_dick:
                save();
                break;
            case R.id.btn_home_push:
                //弹开柜体
                UpOpenLock();
                break;
            case R.id.btn_home_record:
                goActivity(InfoActivity.class);
                break;
            case R.id.btn_home_pairing:
                goActivity(SetWifiActivity.class);
                break;
            case R.id.btn_home_set:
                goActivity(PeopleActivity.class);
                break;
            case R.id.tv_box_name:
                outbox++;
                if (outbox > 5) {
                    outbox = 0;
                    InputDialog.show((AppCompatActivity) context, "提示", "请输入密码", "确定", "取消")
                            .setOnOkButtonClickListener(new OnInputDialogButtonClickListener() {
                                @Override
                                public boolean onClick(BaseDialog baseDialog, View v, String inputStr) {
                                    //inputStr 即当前输入的文本
                                    if (inputStr.equals("sdwm@sdwm")) {
                                        BoxSoUtil.instance().unlockingCover(new BoxSoUtil.BoxCallBack() {
                                            @Override
                                            public void onTimeOut() {

                                            }

                                            @Override
                                            public void onSuccess(int ret) {
                                                Logger.d("成功："+ret);
                                            }

                                            @Override
                                            public void onError(String ret) {
                                                Message msg = new Message();
                                                msg.what = 0;
                                                msg.obj = ret;
                                                handler.sendMessage(msg);
                                            }
                                        });
                                    } else {
                                        MessageDialog.show((AppCompatActivity) context, "提示", "密码错误", "确定");

                                    }
                                    return false;
                                }
                            });
                }
                break;
        }
    }

    private void save() {
        //选择储存模式
        MessageDialog.show((AppCompatActivity) context, "提示", "请选择储存模式", "清单存储","取消", "自由存储")
                .setOnOkButtonClickListener(new OnDialogButtonClickListener() {
                    @Override
                    public boolean onClick(BaseDialog baseDialog, View v) {
                        saveInAli();
                        return false;
                    }
                })
                .setOnCancelButtonClickListener(new OnDialogButtonClickListener() {
                    @Override
                    public boolean onClick(BaseDialog baseDialog, View v) {
                        return false;
                    }
                })
                .setOnOtherButtonClickListener(new OnDialogButtonClickListener() {
                    @Override
                    public boolean onClick(BaseDialog baseDialog, View v) {
                        Config.saveTpye = 1;
                        goActivity(SaveActivity.class);
                        return false;
                    }
                });
    }

    private void saveInAli() {
        // 设备上报
        HashMap<String, ValueWrapper> hashMap = new HashMap<>();
        // TODO 用户根据实际情况设置
        hashMap.put("Tag", new ValueWrapper.IntValueWrapper(0));
        hashMap.put("BoxSn", new ValueWrapper.StringValueWrapper(Config.DeviceName));
        hashMap.put("UserId", new ValueWrapper.StringValueWrapper("测试"));
        OutputParams params = new OutputParams(hashMap);
        LinkKit.getInstance().getDeviceThing().thingEventPost("GetBomEvent", params, new IPublishResourceListener() {
            @Override
            public void onSuccess(String resId, Object o) { // 事件上报成功
                Logger.d("成功" + JSON.toJSON(o));
                Config.event = "thing.service.BomDown";
                Config.saveTpye = 0;
                goActivity(SaveActivity.class);
            }
            @Override
            public void onError(String resId, AError aError) { // 事件上报失败
                Logger.d("失败");
                MessageDialog.show((AppCompatActivity) context, "提示", "事件上报失败", "确定");
            }
        });

    }
    @SuppressLint("HandlerLeak")
    private void initHandler() {
        handler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        MessageDialog.show((AppCompatActivity) context, "警告", (String) msg.obj, "确定");
                        break;
                }
            }
        };
    }

    private void UpOpenLock() {
        //上位机开锁
        BoxSoUtil.instance().unlockingUp(new BoxSoUtil.BoxCallBack() {
            @Override
            public void onTimeOut() {

            }

            @Override
            public void onSuccess(int ret) {
                Date date = new Date(System.currentTimeMillis());
                UserLogEntity userLogEntity = new UserLogEntity(null, BaseApplication.userEntities.getName(), 2
                        , date, BaseApplication.macUp, BaseApplication.macDown);
                DbCore.getDaoSession().getUserLogEntityDao().insert(userLogEntity);
            }

            @Override
            public void onError(String ret) {
                Message msg = new Message();
                msg.what = 0;
                msg.obj = ret;
                handler.sendMessage(msg);
            }
        });
    }

    /**
     * 监听wifi状态
     * 当都没有可用连接，系统会连接已经保存的网路
     */
    public class WifiBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {//wifi连接网络状态变化
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (NetworkInfo.State.DISCONNECTED == info.getState()) {//wifi断开连接
                    binding.tvBoxName.setText("未连接");
                    binding.tvBoxName.setTextColor(Color.RED);
                    binding.imgBoxLed.setImageResource(R.drawable.img_led_red);
                    //判断是否有网络
                    if (NetWorkUtil.isNetworkConnected(context)){
                        binding.tvBoxName.setText("已连接:4G");
                        binding.tvBoxName.setTextColor(Color.GREEN);
                        binding.imgBoxLed.setImageResource(R.drawable.img_led_green);
                    }
                } else if (NetworkInfo.State.CONNECTED == info.getState()) {//wifi连接上了
                    WifiHelper wifiHelper = new WifiHelper(context);
                    binding.tvBoxName.setText("已连接:" + wifiHelper.getCurrentWifiSSID());
                    binding.tvBoxName.setTextColor(Color.GREEN);
                    binding.imgBoxLed.setImageResource(R.drawable.img_led_green);
                } else if (NetworkInfo.State.CONNECTING == info.getState()) {//正在连接
                    binding.tvBoxName.setText("正在连接");
                    binding.tvBoxName.setTextColor(Color.YELLOW);
                    binding.imgBoxLed.setImageResource(R.drawable.img_led_red);
                }
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {  //扫描结果
//                Log.i(TAG, "网络列表变化了");//几秒回调一次
            } else if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {//密码错误

            }
        }
    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        super.finish();

    }
}