package com.kth.safebox.activity;

import android.graphics.Color;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.adapter.TabFragmentPagerAdapter;
import com.kth.safebox.base.BaseActivity;
//import megvii.kth.iotwarehouse.databinding.ActivityInfoBinding;
import com.kth.safebox.fragment.AliListFragment;
import com.kth.safebox.fragment.HistroyFragment;
import com.kth.safebox.fragment.UserFragment;
import com.kth.safebox.util.LinearGradientFontSpan;
//import megvii.testfacepass.databinding.ActivityInfoBinding;

import com.kth.safebox.R;
import com.kth.safebox.databinding.ActivityInfoBinding;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.tab.QMUITabBuilder;
import com.qmuiteam.qmui.widget.tab.QMUITabIndicator;
import com.qmuiteam.qmui.widget.tab.QMUITabSegment;

import java.util.ArrayList;
import java.util.List;

public class InfoActivity extends BaseActivity<ActivityInfoBinding> implements View.OnClickListener {
    private List<Fragment> list;
    private TabFragmentPagerAdapter adapter;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_info;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
        binding.tvInfoTitle.setText(LinearGradientFontSpan.getRadiusGradientSpan(getString(R.string.title), Color.parseColor("#009df4"), Color.parseColor("#00e9d0")));

        QMUITabBuilder builder = binding.tabSegment.tabBuilder();
        binding.tabSegment.addTab(builder.setText("历史记录").setTextSize(24,30).build(context));
        binding.tabSegment.addTab(builder.setText("操作记录").setTextSize(24,30).build(context));
        binding.tabSegment.addTab(builder.setText("设备清单").setTextSize(24,30).build(context));
//        binding.tabSegment.setDefaultTextSize(24,30);
        int indicatorHeight = QMUIDisplayHelper.dp2px(context, 2);
        binding.tabSegment.setIndicator(new QMUITabIndicator(
                indicatorHeight, false, false));

        binding.tabSegment.setupWithViewPager(binding.contentViewPager, false);
        binding.tabSegment.setMode(QMUITabSegment.MODE_FIXED);
        binding.tabSegment.addOnTabSelectedListener(new QMUITabSegment.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int index) {

            }

            @Override
            public void onTabUnselected(int index) {

            }

            @Override
            public void onTabReselected(int index) {
            }

            @Override
            public void onDoubleTap(int index) {
                binding.tabSegment.clearSignCountView(index);
            }
        });
        binding.contentViewPager.setOnPageChangeListener(new MyPagerChangeListener());
        //把Fragment添加到List集合里面
        list = new ArrayList<>();
        list.add(new HistroyFragment());
        list.add(new UserFragment());
        list.add(new AliListFragment());
        adapter = new TabFragmentPagerAdapter(getSupportFragmentManager(), list);
        binding.contentViewPager.setAdapter(adapter);
        binding.contentViewPager.setCurrentItem(0);  //初始化显示第一个页面

    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_info_break:
                finish();
                break;
        }
    }

    /**
     * 设置一个ViewPager的侦听事件，当左右滑动ViewPager时菜单栏被选中状态跟着改变
     */
    public class MyPagerChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {//状态改变时底部对应的字体颜色改变
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }

        }

    }
}