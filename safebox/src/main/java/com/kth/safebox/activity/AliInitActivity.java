package com.kth.safebox.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.aliyun.alink.dm.api.DeviceInfo;
import com.aliyun.alink.dm.api.IThing;
import com.aliyun.alink.dm.model.ResponseModel;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linkkit.api.LinkKitInitParams;
import com.aliyun.alink.linksdk.cmp.connect.hubapi.HubApiRequest;
import com.aliyun.alink.linksdk.cmp.core.base.ARequest;
import com.aliyun.alink.linksdk.cmp.core.base.AResponse;
import com.aliyun.alink.linksdk.cmp.core.listener.IConnectSendListener;
import com.aliyun.alink.linksdk.tmp.api.InputParams;
import com.aliyun.alink.linksdk.tmp.api.OutputParams;
import com.aliyun.alink.linksdk.tmp.device.payload.ValueWrapper;
import com.aliyun.alink.linksdk.tmp.devicemodel.Service;
import com.aliyun.alink.linksdk.tmp.listener.ITResRequestHandler;
import com.aliyun.alink.linksdk.tmp.listener.ITResResponseCallback;
import com.aliyun.alink.linksdk.tmp.utils.ErrorInfo;
import com.aliyun.alink.linksdk.tools.AError;
//import megvii.kth.iotwarehouse.R;
import com.kth.safebox.base.BaseActivity;
import com.kth.safebox.base.Config;
//import megvii.kth.iotwarehouse.databinding.ActivityAliInitBinding;
import com.kth.safebox.databinding.ActivityAliInitBinding;
import com.kth.safebox.manager.IDemoCallback;
import com.kth.safebox.manager.InitManager;
//import megvii.testfacepass.databinding.ActivityAliInitBinding;

import com.kth.safebox.R;
import com.kth.safebox.util.JSONUtils;
import com.kth.safebox.util.LinearGradientFontSpan;
import com.orhanobut.logger.Logger;
import com.testfacepass.SplashActivity;
import com.testfacepass.utils.TxtTool;
import com.xuhao.didi.socket.common.interfaces.utils.TextUtils;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

public class AliInitActivity extends BaseActivity<ActivityAliInitBinding> {
    private String productKey = Config.ProductKey;
    private String deviceName = Config.DeviceName;
    private String productSecret = Config.ProductSecret;
    private String deviceSecret = Config.DeviceSecret;
    private final static String SERVICE_SET = "set";
    private final static String SERVICE_GET = "get";
    private TextView textView;

    public AliInitActivity() {
        resRequestHandler = new ResRequestHandler(mCommonHandler);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_ali_init;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        textView = findViewById(R.id.tv_main_title);
        textView.setText(LinearGradientFontSpan.getRadiusGradientSpan(getString(R.string.title), Color.parseColor("#009df4"), Color.parseColor("#00e9d0")));

    }

    @Override
    protected void initView() {

    }

    @SuppressLint("HandlerLeak")
    @Override
    protected void initData() {
        String s = TxtTool.readTxt("/sdcard/三元码.txt");
        KeyAuthentication(s);
    }

    //密钥认证
    private void KeyAuthentication(String s) {
        /**
         * 设置设备三元组信息
         */
        DeviceInfo deviceInfo = JSONUtils.fromJson(s, DeviceInfo.class);
        if (deviceInfo == null) {
            deviceInfo = new DeviceInfo();
            deviceInfo.productKey = productKey;// 产品类型
            deviceInfo.deviceName = deviceName;// 设备名称
            deviceInfo.deviceSecret = deviceSecret;// 设备密钥
        }

        /**
         * 设备初始化建联
         * onError 初始化建联失败，需要用户重试初始化。如因网络问题导致初始化失败。
         * onInitDone 初始化成功
         */
        InitManager.init(this, deviceInfo.productKey, deviceInfo.deviceName,
                deviceInfo.deviceSecret, deviceInfo.productSecret, new IDemoCallback() {
                    @Override
                    public void onError(AError aError) {
                        Logger.d("初始化失败:onError() called with: aError = [" + InitManager.getAErrorString(aError) + "]");
                        // 初始化失败，初始化失败之后需要用户负责重新初始化
                        // 如一开始网络不通导致初始化失败，后续网络恢复之后需要重新初始化
//                        KeyAuthentication(s);
                    }

                    @Override
                    public void onInitDone(Object data) {
                        Logger.d("初始化成功:onInitDone() called with: data = [" + data + "]");
                        // 获取所有属性
                        Config.propertyList = LinkKit.getInstance().getDeviceThing().getProperties();
                        // 获取设备支持的所有服务
                        Config.srviceList = LinkKit.getInstance().getDeviceThing().getServices();
                        //获取设备的事件列表
                        Config.eventList = LinkKit.getInstance().getDeviceThing().getEvents();
                        //上传事件测试
//                PropertiesReported(Config.eventList.get(2));
                        //同异步调用测试
                        setServiceHandler();
//                        PropertiesReported();
                        startActivity(new Intent(context, SplashActivity.class));


                    }
                });
    }

    //动态注册
    private void DynamicRegistration() {
        //  #######  一型一密动态注册接口开始 ######
        /**
         * 注意：动态注册成功，设备上线之后，不能再次执行动态注册，云端会返回已注册错误信息。
         *   因此用户在编程时首先需要判断设备是否已获取过deviceSecret，没有获取过的情况下再
         *   调用动态注册接口去获取deviceSecret
         */
        DeviceInfo deviceInfo = new DeviceInfo();

        DeviceInfo myDeviceInfo = new DeviceInfo();
        myDeviceInfo.productKey = productKey;
        myDeviceInfo.deviceName = deviceName;
        myDeviceInfo.productSecret = productSecret;
        LinkKitInitParams params = new LinkKitInitParams();
        params.deviceInfo = myDeviceInfo;
        // 设置动态注册请求 path 和 域名，域名使用默认即可
        HubApiRequest hubApiRequest = new HubApiRequest();
        hubApiRequest.path = "/auth/register/device";
        LinkKit.getInstance().deviceRegister(context, params, hubApiRequest, new IConnectSendListener() {
            @Override
            public void onResponse(ARequest aRequest, AResponse aResponse) {
                // aRequest 用户的请求数据
                if (aResponse != null && aResponse.data != null) {
                    ResponseModel<Map<String, String>> response = JSONObject.parseObject(aResponse.data.toString(),
                            new TypeReference<ResponseModel<Map<String, String>>>() {
                            }.getType());
                    if ("200".equals(response.code) && response.data != null && response.data.containsKey("deviceSecret") &&
                            !TextUtils.isEmpty(response.data.get("deviceSecret"))) {
                        deviceInfo.deviceSecret = response.data.get("deviceSecret");
                        // getDeviceSecret success, to build connection.
                        // 持久化 deviceSecret 初始化建联的时候需要
                        // TODO  用户需要按照实际场景持久化设备的三元组信息，用于后续的连接
                        // 成功获取 deviceSecret，调用初始化接口建联
                        // TODO 调用设备初始化建联
                    }
                }
            }

            @Override
            public void onFailure(ARequest aRequest, AError aError) {
                Log.d("TAG", "onFailure() called with: aRequest = [" + aRequest + "], aError = [" + aError + "]");
            }
        });
        //  ####### 一型一密动态注册接口结束  ######
    }

    /**
     * 云端调用设备的某项服务的时候，设备端需要响应该服务并回复。
     * 设备端事件触发的时候需要调用这个接口上报事件，如事件告警等
     * 需要用户在云端定义不同的 Error 的类型
     */
    private void setServiceHandler() {
        IThing thing = LinkKit.getInstance().getDeviceThing();


        List<Service> srviceList = thing.getServices();
        for (int i = 0; srviceList != null && i < srviceList.size(); i++) {
            Service service = srviceList.get(i);
            thing.setServiceHandler(service.getIdentifier(), resRequestHandler);
        }
        //
    }

    private ResRequestHandler resRequestHandler;

    static class ResRequestHandler implements ITResRequestHandler {
        WeakReference<ITResRequestHandler> handlerWakRef = null;

        public ResRequestHandler(ITResRequestHandler handler) {
            handlerWakRef = new WeakReference<>(handler);
        }

        @Override
        public void onProcess(String s, Object o, ITResResponseCallback itResResponseCallback) {
            if (handlerWakRef != null && handlerWakRef.get() != null) {
                handlerWakRef.get().onProcess(s, o, itResResponseCallback);
            }
        }

        @Override
        public void onSuccess(Object o, OutputParams outputParams) {
            if (handlerWakRef != null && handlerWakRef.get() != null) {
                handlerWakRef.get().onSuccess(o, outputParams);
            }
        }

        @Override
        public void onFail(Object o, ErrorInfo errorInfo) {
            if (handlerWakRef != null && handlerWakRef.get() != null) {
                handlerWakRef.get().onFail(o, errorInfo);
            }
        }
    }

    private ITResRequestHandler mCommonHandler = new ITResRequestHandler() {
        @Override
        public void onProcess(String identify, Object result, ITResResponseCallback itResResponseCallback) {

            Logger.d("收到异步服务调用 " + identify);
            try {
                if (SERVICE_SET.equals(identify)) {
                    // TODO  用户按照真实设备的接口调用  设置设备的属性
                    // 设置完真实设备属性之后，上报设置完成的属性值
                    // 用户根据实际情况判断属性是否设置成功 这里测试直接返回成功
                    boolean isSetPropertySuccess = true;
                    if (isSetPropertySuccess) {
                        if (result instanceof InputParams) {
                            Map<String, ValueWrapper> data = (Map<String, ValueWrapper>) ((InputParams) result).getData();
//                        data.get()
                            // 响应云端 接收数据成功
                            itResResponseCallback.onComplete(identify, null, null);
                        } else {
                            itResResponseCallback.onComplete(identify, null, null);
                        }
//                        updatePropertyValue((Property) mPropertySpinner.getSelectedItem());
                    } else {
                        AError error = new AError();
                        error.setCode(100);
                        error.setMsg("setPropertyFailed.");
                        itResResponseCallback.onComplete(identify, new ErrorInfo(error), null);
                    }

                } else if (SERVICE_GET.equals(identify)) {
                    //  初始化的时候将默认值初始化传进来，物模型内部会直接返回云端缓存的值

                } else {
                    // 根据不同的服务做不同的处理，跟具体的服务有关系
                    Logger.d("用户根据真实的服务返回服务的值，请参照set示例");
                    OutputParams outputParams = new OutputParams();
//                    outputParams.put("op", new ValueWrapper.IntValueWrapper(20));
                    itResResponseCallback.onComplete(identify, null, outputParams);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Logger.d("TMP 返回数据格式异常");
            }
        }

        @Override
        public void onSuccess(Object o, OutputParams outputParams) {
            Logger.d("注册服务成功：onSuccess() called with: o = [" + o + "], outputParams = [" + outputParams + "]");
        }

        @Override
        public void onFail(Object o, ErrorInfo errorInfo) {
            Logger.d("注册服务失败：onFail() called with: o = [" + o + "], errorInfo = [" + errorInfo + "]");
        }
    };

}