package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 用户操作数据表
 * */
@Entity
public class UserLogEntity {
    @Id
    private Long id;
    private String userId;//操作人员id
    private int msgType;//操作类型 1存入，2取出
    private Date time;//操作时间
    private String upperMac;//上位机mac
    private String underMac;//下位机Mac
    @Generated(hash = 1639064128)
    public UserLogEntity(Long id, String userId, int msgType, Date time,
            String upperMac, String underMac) {
        this.id = id;
        this.userId = userId;
        this.msgType = msgType;
        this.time = time;
        this.upperMac = upperMac;
        this.underMac = underMac;
    }
    @Generated(hash = 1237015982)
    public UserLogEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUserId() {
        return this.userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public int getMsgType() {
        return this.msgType;
    }
    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }
    public Date getTime() {
        return this.time;
    }
    public void setTime(Date time) {
        this.time = time;
    }
    public String getUpperMac() {
        return this.upperMac;
    }
    public void setUpperMac(String upperMac) {
        this.upperMac = upperMac;
    }
    public String getUnderMac() {
        return this.underMac;
    }
    public void setUnderMac(String underMac) {
        this.underMac = underMac;
    }


}
