package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 柜体数据表
 * */
@Entity
public class WifiNameEntity {
    @Id
    private Long id;
    private String ssid;//
    private String password;//操作类型 1存入，2取出
    @Generated(hash = 1525333194)
    public WifiNameEntity(Long id, String ssid, String password) {
        this.id = id;
        this.ssid = ssid;
        this.password = password;
    }
    @Generated(hash = 686299248)
    public WifiNameEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getSsid() {
        return this.ssid;
    }
    public void setSsid(String ssid) {
        this.ssid = ssid;
    }
    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
