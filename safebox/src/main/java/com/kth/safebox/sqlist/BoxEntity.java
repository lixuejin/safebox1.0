package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 柜体数据表
 * */
@Entity
public class BoxEntity {
    @Id
    private Long id;
    private String boxMac;
    private String boxName;
    private Date inTime;//存入时间
    private Date outTime;//取出时间
    @Generated(hash = 1679404195)
    public BoxEntity(Long id, String boxMac, String boxName, Date inTime,
            Date outTime) {
        this.id = id;
        this.boxMac = boxMac;
        this.boxName = boxName;
        this.inTime = inTime;
        this.outTime = outTime;
    }
    @Generated(hash = 1205622765)
    public BoxEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getBoxMac() {
        return this.boxMac;
    }
    public void setBoxMac(String boxMac) {
        this.boxMac = boxMac;
    }
    public String getBoxName() {
        return this.boxName;
    }
    public void setBoxName(String boxName) {
        this.boxName = boxName;
    }
    public Date getInTime() {
        return this.inTime;
    }
    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }
    public Date getOutTime() {
        return this.outTime;
    }
    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

}
