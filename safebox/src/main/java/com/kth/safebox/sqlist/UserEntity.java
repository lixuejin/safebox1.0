package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
/**
 * 用户数据表
 * */
@Entity
public class UserEntity {
    @Id
    private Long id;
    private String name;
    private String face;//float[]转string
    @Generated(hash = 821898388)
    public UserEntity(Long id, String name, String face) {
        this.id = id;
        this.name = name;
        this.face = face;
    }
    @Generated(hash = 1433178141)
    public UserEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFace() {
        return this.face;
    }
    public void setFace(String face) {
        this.face = face;
    }

}
