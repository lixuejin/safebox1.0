package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 硬盘数据表
 * */
@Entity
public class ErrorEntity {
    @Id
    private Long id;
    private String type;//事件类型
    private int userId;//操作人员id
    private Date time;//发生时间
    private String errorMsg;//异常原因
    @Generated(hash = 1425844585)
    public ErrorEntity(Long id, String type, int userId, Date time,
            String errorMsg) {
        this.id = id;
        this.type = type;
        this.userId = userId;
        this.time = time;
        this.errorMsg = errorMsg;
    }
    @Generated(hash = 1978847478)
    public ErrorEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public int getUserId() {
        return this.userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public Date getTime() {
        return this.time;
    }
    public void setTime(Date time) {
        this.time = time;
    }
    public String getErrorMsg() {
        return this.errorMsg;
    }
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
