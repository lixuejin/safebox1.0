package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;

/**
 * 连接记录数据表
 * */
@Entity(indexes = {
        @Index(value = "underMac", unique = true)
})
public class PairEntity {
    @Id
    private Long id;
    private Date time;//操作时间
    private String upperMac;//上位机mac
    private String upperIp;//上位机ip
    private String underMac;//下位机Mac
    private String underIp;//下位机ip
    private String upperNmae;//上位机名
    private String underName;//下位机名
    private Boolean isPair;//是否连接成功
    @Generated(hash = 1078505955)
    public PairEntity(Long id, Date time, String upperMac, String upperIp,
            String underMac, String underIp, String upperNmae, String underName,
            Boolean isPair) {
        this.id = id;
        this.time = time;
        this.upperMac = upperMac;
        this.upperIp = upperIp;
        this.underMac = underMac;
        this.underIp = underIp;
        this.upperNmae = upperNmae;
        this.underName = underName;
        this.isPair = isPair;
    }
    @Generated(hash = 904173116)
    public PairEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Date getTime() {
        return this.time;
    }
    public void setTime(Date time) {
        this.time = time;
    }
    public String getUpperMac() {
        return this.upperMac;
    }
    public void setUpperMac(String upperMac) {
        this.upperMac = upperMac;
    }
    public String getUpperIp() {
        return this.upperIp;
    }
    public void setUpperIp(String upperIp) {
        this.upperIp = upperIp;
    }
    public String getUnderMac() {
        return this.underMac;
    }
    public void setUnderMac(String underMac) {
        this.underMac = underMac;
    }
    public String getUnderIp() {
        return this.underIp;
    }
    public void setUnderIp(String underIp) {
        this.underIp = underIp;
    }
    public String getUpperNmae() {
        return this.upperNmae;
    }
    public void setUpperNmae(String upperNmae) {
        this.upperNmae = upperNmae;
    }
    public String getUnderName() {
        return this.underName;
    }
    public void setUnderName(String underName) {
        this.underName = underName;
    }
    public Boolean getIsPair() {
        return this.isPair;
    }
    public void setIsPair(Boolean isPair) {
        this.isPair = isPair;
    }


}
