package com.kth.safebox.sqlist;

import com.kth.safebox.sqlist.converter.InterConverter;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 存放批次数据表
 * */
@Entity
public class PutListEntity {
    @Id
    private Long id;
    private String lsitName;
    @Convert(columnType = String.class, converter = InterConverter.class)
    private List<Integer> diskList;//本次存入的硬盘的id
    private Date inTime;//存入时间
    private String upperNmae;//上位机名
    private String upperMac;//上位机mac
    private String underName;//下位机名
    private String underMac;//下位机Mac
    private Boolean fromALi;//属于阿里设备清单
    @Generated(hash = 1321462788)
    public PutListEntity(Long id, String lsitName, List<Integer> diskList,
            Date inTime, String upperNmae, String upperMac, String underName,
            String underMac, Boolean fromALi) {
        this.id = id;
        this.lsitName = lsitName;
        this.diskList = diskList;
        this.inTime = inTime;
        this.upperNmae = upperNmae;
        this.upperMac = upperMac;
        this.underName = underName;
        this.underMac = underMac;
        this.fromALi = fromALi;
    }
    @Generated(hash = 50880920)
    public PutListEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLsitName() {
        return this.lsitName;
    }
    public void setLsitName(String lsitName) {
        this.lsitName = lsitName;
    }
    public List<Integer> getDiskList() {
        return this.diskList;
    }
    public void setDiskList(List<Integer> diskList) {
        this.diskList = diskList;
    }
    public Date getInTime() {
        return this.inTime;
    }
    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }
    public String getUpperNmae() {
        return this.upperNmae;
    }
    public void setUpperNmae(String upperNmae) {
        this.upperNmae = upperNmae;
    }
    public String getUpperMac() {
        return this.upperMac;
    }
    public void setUpperMac(String upperMac) {
        this.upperMac = upperMac;
    }
    public String getUnderName() {
        return this.underName;
    }
    public void setUnderName(String underName) {
        this.underName = underName;
    }
    public String getUnderMac() {
        return this.underMac;
    }
    public void setUnderMac(String underMac) {
        this.underMac = underMac;
    }
    public Boolean getFromALi() {
        return this.fromALi;
    }
    public void setFromALi(Boolean fromALi) {
        this.fromALi = fromALi;
    }


}
