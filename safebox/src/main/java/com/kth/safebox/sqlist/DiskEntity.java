package com.kth.safebox.sqlist;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;
/**
 * 硬盘数据表
 * */
@Entity
public class DiskEntity {
    @Id
    private Long id;
    private String idNumber;//产品序列号
    private String ticketNo;//工单编号
    private Date inTime;//存入时间
    private String upperMac;//上位机mac
    private String underMac;//下位机Mac
    private Boolean fromALi;//属于阿里设备清单
    @Generated(hash = 448432917)
    public DiskEntity(Long id, String idNumber, String ticketNo, Date inTime,
            String upperMac, String underMac, Boolean fromALi) {
        this.id = id;
        this.idNumber = idNumber;
        this.ticketNo = ticketNo;
        this.inTime = inTime;
        this.upperMac = upperMac;
        this.underMac = underMac;
        this.fromALi = fromALi;
    }
    @Generated(hash = 1626761515)
    public DiskEntity() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getIdNumber() {
        return this.idNumber;
    }
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    public Date getInTime() {
        return this.inTime;
    }
    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }
    public String getUpperMac() {
        return this.upperMac;
    }
    public void setUpperMac(String upperMac) {
        this.upperMac = upperMac;
    }
    public String getUnderMac() {
        return this.underMac;
    }
    public void setUnderMac(String underMac) {
        this.underMac = underMac;
    }
    public Boolean getFromALi() {
        return this.fromALi;
    }
    public void setFromALi(Boolean fromALi) {
        this.fromALi = fromALi;
    }
    public String getTicketNo() {
        return this.ticketNo;
    }
    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }


}
