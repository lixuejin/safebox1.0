/*
 * Tencent is pleased to support the open source community by making QMUI_Android available.
 *
 * Copyright (C) 2017-2018 THL A29 Limited, a Tencent company. All rights reserved.
 *
 * Licensed under the MIT License (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.kth.safebox.model;

import com.kth.safebox.sqlist.DiskEntity;
import com.qmuiteam.qmui.widget.section.QMUISection;

import java.util.Objects;

public class SectionItem implements QMUISection.Model<SectionItem> {
    private final DiskEntity diskEntity;

    public SectionItem(DiskEntity diskEntity){
        this.diskEntity = diskEntity;
    }

    public DiskEntity getDiskEntity() {
        return diskEntity;
    }

    @Override
    public SectionItem cloneForDiff() {
        return new SectionItem(getDiskEntity());
    }

    @Override
    public boolean isSameItem(SectionItem other) {
        return Objects.equals(diskEntity, other.diskEntity);
    }

    @Override
    public boolean isSameContent(SectionItem other) {
        return true;
    }
}
