package com.kth.safebox.util;

public class StringUtil {
    public static  String floatToString(float[] fs) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < fs.length; i++) {
            stringBuilder.append(fs[i] + " ");
        }
        String s = stringBuilder.toString();
        return s;
    }

    public static float[] stringToFloat(String string) {
        String[] strings = string.split(" ");
        float[] fs = new float[strings.length];
        for (int i = 0; i < strings.length; i++) {
            fs[i] = Float.parseFloat(strings[i]);
        }
        return fs;
    }
//    public static List<String> listuse(){
//
//    }
}
