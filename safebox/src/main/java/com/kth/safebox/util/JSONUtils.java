package com.kth.safebox.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class JSONUtils {
    public static String parser2json(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    public static String parserDateBean2json(Object obj) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder
                .registerTypeAdapter(Date.class,
                        new DateSerializerUtils())
                .setDateFormat(DateFormat.LONG).create();
        return gson.toJson(obj);
    }

    public static <T> List<T> fromJson(String strjson) {
        try {
            Gson gs = new Gson();
            Type listType = new TypeToken<List<T>>() {
            }.getType();
            List<T> target2 = gs.fromJson(strjson, listType);
            return target2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T fromJson(String strjson, Class<T> cls) {
        try {
            Gson gs = new Gson();
            T target2 = (T) gs.fromJson(strjson, cls);
            return target2;
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T fromJson2DateBean(String json, Class<T> cls) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder
                .registerTypeAdapter(Date.class,
                        new DateDeserializerUtils())
                .setDateFormat(DateFormat.LONG).create();
        return gson.fromJson(json, cls);
    }

    private static class DateDeserializerUtils implements
            JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement json, Type type,
                                          JsonDeserializationContext context) throws JsonParseException {
            return new Date(json.getAsJsonPrimitive().getAsLong());
        }

    }

    private static class DateSerializerUtils implements
            JsonSerializer<Date> {
        @Override
        public JsonElement serialize(Date date, Type type,
                                     JsonSerializationContext content) {
            return new JsonPrimitive(date.getTime());
        }
    }
}
