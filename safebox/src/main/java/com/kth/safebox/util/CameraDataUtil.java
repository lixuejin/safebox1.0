package com.kth.safebox.util;

import android.hardware.Camera;
import android.util.Log;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CameraDataUtil {
    private static List<Camera.Size> supportedVideoSizes;
    private static List<Camera.Size> previewSizes;

    public static String getCameraInfo(){

        int cameracount = 0;//摄像头数量
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();  //获取摄像头信息
        cameracount = Camera.getNumberOfCameras();
        Log.i("CameraTest","摄像头数量"+String.valueOf(cameracount));
        for(int cameraId=0; cameraId<Camera.getNumberOfCameras(); cameraId++)
        {
            Camera.getCameraInfo( cameraId, cameraInfo);
            Camera camera = Camera.open(cameraId); //开启摄像头获得一个Camera的实例
            Camera.Parameters params = camera.getParameters();  //通过getParameters获取参数
            supportedVideoSizes = params.getSupportedPictureSizes();
            previewSizes = params.getSupportedPreviewSizes();
            camera.release();//释放摄像头

            //重新排列后设下摄像头预设分辨率在所有分辨率列表中的地址，用以选择最佳分辨率（保证适配不出错）
            int index=bestVideoSize(previewSizes.get(0).width);
            Log.i("CameraTest", "预览分辨率地址: " + index );
            if (null != previewSizes && previewSizes.size() > 0){  //判断是否获取到值，否则会报空对象
                Log.i("CameraTest", "摄像头最高分辨率宽: " + String.valueOf(supportedVideoSizes.get(0).width) );  //降序后取最高值，返回的是int类型
                Log.i("CameraTest", "摄像头最高分辨率高: " + String.valueOf(supportedVideoSizes.get(0).height) );
                Log.i("CameraTest", "摄像头分辨率全部: " + cameraSizeToSting( supportedVideoSizes) );
            }else{
                Log.i("CameraTest", "没取到值啊什么鬼" );
                Log.i("CameraTest", "摄像头预览分辨率: " + String.valueOf(previewSizes.get(0).width) );
            }
        }
        return cameraSizeToSting( supportedVideoSizes );
    }

    //重新排列获取到的分辨率列表
    private static int bestVideoSize(int _wid) {

        //降序排列
        Collections.sort(supportedVideoSizes, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size lhs, Camera.Size rhs) {
                if (lhs.width > rhs.width) {
                    return -1;
                } else if (lhs.width == rhs.width) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        for (int i = 0; i < supportedVideoSizes.size(); i++) {
            if (supportedVideoSizes.get(i).width < _wid) {
                return i;
            }
        }
        return 0;
    }

    //分辨率格式化输出String值
    private static String cameraSizeToSting(Iterable<Camera.Size> sizes)
    {
        StringBuilder s = new StringBuilder();
        for (Camera.Size size : sizes)
        {
            if (s.length() != 0)
                s.append(",\n");
            s.append(size.width).append('x').append(size.height);
        }
        return s.toString();
    }
}
