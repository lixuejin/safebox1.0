package com.kth.safebox.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class ToolKits {

    public static SharedPreferences getSharedPerferences(Context context) {
        return context.getSharedPreferences("megvii.kth.iotwarehouse", Context.MODE_PRIVATE);//设计为私有模式
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences sharedPerferences = getSharedPerferences(context);
        //获取共享参数的编辑器
        SharedPreferences.Editor editor = sharedPerferences.edit();
        //通过编辑器提交一个boolean类型的参数
        editor.putInt(key, value);
        editor.commit();
    }

    //获取共享参数
    public static int GetInt(Context context, String key, int defaultValue) {
        return getSharedPerferences(context).getInt(key, defaultValue);
    }


    //设置共享参数
    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences sharedPerferences = getSharedPerferences(context);
        //获取共享参数的编辑器
        SharedPreferences.Editor editor = sharedPerferences.edit();
        //通过编辑器提交一个boolean类型的参数
        editor.putBoolean(key, value);
        editor.commit();
    }


    //获取共享参数
    public static boolean GetBoolean(Context context, String key, boolean defaultValue) {
        return getSharedPerferences(context).getBoolean(key, defaultValue);
    }

    /**
     * 获取设备的Mac地址
     *
     * @return Mac地址
     */
    public static String getLocalMacAddress() {
        String Mac = null;
        try {
            String path = "sys/class/net/wlan0/address";
            if ((new File(path)).exists()) {
                FileInputStream fis = new FileInputStream(path);
                byte[] buffer = new byte[8192];
                int byteCount = fis.read(buffer);
                if (byteCount > 0) {
                    Mac = new String(buffer, 0, byteCount, "utf-8");
                }
            }
            Log.v("sdfdsfew", "" + Mac);
            if (Mac == null || Mac.length() == 0) {
                path = "sys/class/net/eth0/address";
                FileInputStream fis_name = new FileInputStream(path);
                byte[] buffer_name = new byte[8192];
                int byteCount_name = fis_name.read(buffer_name);
                if (byteCount_name > 0) {
                    Mac = new String(buffer_name, 0, byteCount_name, "utf-8");
                }
            }
            Log.d("fddsfds", "" + Mac);
            if (Mac.length() == 0 || Mac == null) {
                return "";
            }
        } catch (Exception io) {
            Log.v("daming.zou**exception*", "" + io.toString());
        }

        Log.v("xulongheng*Mac", "" + Mac);
        return "" + Mac.trim();

    }

    public static String getMacAddress() {
        List<NetworkInterface> interfaces = null;
        try {
            interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                if (networkInterface != null && TextUtils.isEmpty(networkInterface.getName()) == false) {
                    if ("wlan0".equalsIgnoreCase(networkInterface.getName())) {
                        byte[] macBytes = networkInterface.getHardwareAddress();
                        if (macBytes != null && macBytes.length > 0) {
                            StringBuilder str = new StringBuilder();
                            for (byte b : macBytes) {
                                str.append(String.format("%02X:", b));
                            }
                            if (str.length() > 0) {
                                str.deleteCharAt(str.length() - 1);
                            }
                            return str.toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "unknown";
    }


    //    获取WiFi连接的ip地址
    public static String getWifiIp(Context myContext) {

        if (myContext == null) {
            throw new NullPointerException("Global context is null");
        }
        WifiManager wifiMgr = (WifiManager) myContext.getSystemService(Context.WIFI_SERVICE);
//        if (isWifiEnabled()) {
            int ipAsInt = wifiMgr.getConnectionInfo().getIpAddress();
            if (ipAsInt == 0) {
                return null;
            } else {
                return intToInet(ipAsInt);
            }
//        } else {
//            return null;
//        }
    }

    //将获取的int转为真正的ip地址,参考的网上的，修改了下
    private static String intToInet(int i) {
       String name=(i & 0xFF) + "."+ ((i >> 8) & 0xFF)  +"." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
        return name;
    }

    //获取本机ip地址
    public static String getIP(Context context) {

        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && (inetAddress instanceof Inet4Address)) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}