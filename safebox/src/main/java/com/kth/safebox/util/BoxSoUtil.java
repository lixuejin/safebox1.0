package com.kth.safebox.util;

import com.sdwm.safebox.SafeBoxOne;

import java.util.List;

public class BoxSoUtil {
    //    private static int ERR_IO = 1;//IO版通讯异常
//    private static int ERR_SCAN = 2;//条码枪通讯异常
//    private static int ERR_GRAITHAVE = 3;//光栅有遮挡
//    private static int ERR_NODOWNSAFEBOX = 4; //下部安全箱未到位
//    private static int ERR_NOSTART = 5;//用户未按启动按钮
//    private static int ERR_UPOPEN = 6; //上部门打开失败
//    private static int ERR_UPCLOSE = 7; //上部门关闭失败
//    private static int ERR_PUSH = 8;//下推失败
//    private static int ERR_UPLOCK = 9; //上部开锁失败
//    private static int ERR_DOWNLOCK = 10;//下部开锁失败
//    private static int ERR_DOWNOPEN = 11; //下部开门失败
//    private static int ERR_PUSHBACK = 12; //下推复位失败
//    private static int ERR_LINEONE = 13; //直线1定位异常
//    private static int ERR_LINETWO = 14; //直线2定位异常
//    private static int ERR_LINETHREE = 15;//直线3定位异常
//    private static int ERR_SCAN_NOREAD = 16; //条码枪没扫到数据
    private static BoxSoUtil instance;

    private BoxSoUtil() {

    }

    public static synchronized BoxSoUtil instance() {
        if (instance == null) {
            instance = new BoxSoUtil();
        }
        return instance;
    }

    //上位机初始化
    public void initUp(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.init(SafeBoxOne.ioport, SafeBoxOne.scanUp, SafeBoxOne.scanUpTwo, SafeBoxOne.scanSide,"");
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }

    //推入硬盘扫码-多码-人工
    public void putOnDisk(final BoxScanCallBack BoxScanCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int[] result = new int[52];
                byte[] rfidCode = new byte[52];
                int[] rfidCount = new int[52];
                List<String> list = SafeBoxOne.putOnDisk(result, rfidCode, rfidCount);
                if (result[0] == 0) {

                    BoxScanCallBack.onSuccess(list,rfidCode);
                } else {
                    BoxScanCallBack.onError(errorMsg(result[0]));
                }
            }
        }).start();
    }

    //推入硬盘扫码-多码-机器人
    public void robotGoOn(final BoxScanCallBack BoxScanCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int[] result = new int[52];
                byte[] rfidCode = new byte[52];
                int[] rfidCount = new int[52];
                List<String> list = SafeBoxOne.robotGoOn(result, rfidCode, rfidCount);
                if (result[0] == 0) {
                    BoxScanCallBack.onSuccess(list,rfidCode);
                } else {
                    BoxScanCallBack.onError(errorMsg(result[0]));
                }
            }
        }).start();
    }
    //返回
    public void back(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.back();
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }
    //获取是否合体
    public void getStatus(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.getStatus();
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }

    //开锁取箱子
    public void unlockingUp(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.unlocking();
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }

    //上位机复位
    public void upReset(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.upReset();
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }

    //盖子开锁
    public void unlockingCover(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.unlockingCover();
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }

    //推下硬盘
    public void pushDownDisk(final BoxCallBack BoxCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret = SafeBoxOne.pushDownDisk();
                if (ret == 0) {
                    BoxCallBack.onSuccess(ret);
                } else {
                    BoxCallBack.onError(errorMsg(ret));
                }
            }
        }).start();
    }
    private String errorMsg(int ret) {
        String msg = "";
        switch (ret) {
            case 1:
                msg = "IO版通讯异常";
                break;
            case 2:
                msg = "条码枪通讯异常";
                break;
            case 3:
                msg = "光栅有遮挡";
                break;
            case 4:
                msg = "下部安全箱未到位";
                break;
            case 5:
                msg = "用户未按启动按钮";
                break;
            case 6:
                msg = "上部门打开失败";
                break;
            case 7:
                msg = "上部门关闭失败";
                break;
            case 8:
                msg = "下推失败";
                break;
            case 9:
                msg = "上部开锁失败";
                break;
            case 10:
                msg = "下部开锁失败";
                break;
            case 11:
                msg = "下部开门失败";
                break;
            case 12:
                msg = "下推复位失败";
                break;
            case 13:
                msg = "直线1定位异常";
                break;
            case 14:
                msg = "直线2定位异常";
                break;
            case 15:
                msg = "直线3定位异常";
                break;
            case 16:
                msg = "条码枪没扫到数据";
                break;
            case 17:
                msg = "下部关门失败";
                break;
            case 18:
                msg = "堆料已满";
                break;
            case 19:
                msg = "系统错误";
                break;
            case 20:
                msg = "自锁关门失败";
                break;
            case 21:
                msg = "自锁开门失败";
                break;
            default:
                msg = "其他错误";
                break;
        }
        return msg;
    }

    public interface BoxCallBack {
        void onTimeOut();

        void onSuccess(int ret);

        void onError(String ret);// 检测到人脸框
    }

    public interface BoxScanCallBack {
        void onTimeOut();

        void onSuccess(List<String> list, byte[] rfidCode);

        void onError(String ret);// 检测到人脸框
    }
}
