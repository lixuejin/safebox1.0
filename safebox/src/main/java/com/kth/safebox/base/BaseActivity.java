package com.kth.safebox.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity<VDB extends ViewDataBinding> extends AppCompatActivity {
    protected VDB binding;

    //获取当前activity布局文件
    protected abstract int getContentViewId();

    protected Context context;
    @NonNull
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getContentViewId());
        binding.setLifecycleOwner(this);
        context = this;
        initView();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    protected abstract void initView();

    protected abstract void initData();


    protected void goActivity(Class<?> activity) {
        startActivity(new Intent(this, activity));
        overridePendingTransition(0, 0);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }
}