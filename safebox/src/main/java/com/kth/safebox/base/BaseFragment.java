package com.kth.safebox.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;



public abstract class BaseFragment<DB extends ViewDataBinding> extends Fragment {
    protected DB binding;
    public abstract int getLayoutId();
    protected abstract void onBindView(View view, ViewGroup container, Bundle savedInstanceState);
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(this.getContext()).inflate(getLayoutId(), null);
        binding = DataBindingUtil.bind(view);
        onBindView(view, container, savedInstanceState);
        initView();
        initData();
        return view;
    }
    protected abstract void initView();
    protected abstract void initData();
}