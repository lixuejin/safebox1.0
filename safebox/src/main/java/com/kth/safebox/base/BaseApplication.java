package com.kth.safebox.base;

import android.app.Application;

import com.kongzue.dialog.util.DialogSettings;

import com.kth.safebox.sqlist.UserEntity;
import com.kth.safebox.util.CrashHandler;
import com.kth.safebox.util.DbCore;
import com.kth.safebox.util.ToolKits;
import com.kth.safebox.util.saveSPUtil;

import mcv.facepass.FacePassHandler;

import static com.kongzue.dialog.util.DialogSettings.STYLE.STYLE_KONGZUE;

/**
 * @author lxj
 */
public class BaseApplication extends Application {
    public static UserEntity userEntities;
    public static final boolean IS_DEBUG = false;
    public static String receiverIp;
    public static String name = "未命名";
    public static int msgList = 0;
    public static boolean LinkKit;
    private static BaseApplication instance;
    public static String macUp = "";
    public static String macDown = "";
    public static BaseApplication getInstance() {
        if (instance == null) {
            instance = new BaseApplication();
        }
        return instance;
    }
    public static FacePassHandler mFacePassHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        DbCore.init(this);
        CrashHandler.getInstance().init(this);
        initData();
    }

    private void initData() {
        userEntities = new UserEntity(null, "测试", "");
        Config.mac = ToolKits.getMacAddress();
        name = saveSPUtil.getString(this,"wifiName");
       if (name.equals("")){
           name = "未命名";
       }
        DialogSettings.style = STYLE_KONGZUE;
        DialogSettings.cancelable = true;//全局对话框默认是否可以点击外围遮罩区域或返回键关闭

    }

}
