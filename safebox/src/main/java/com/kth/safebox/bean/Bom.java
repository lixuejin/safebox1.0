/**
  * Copyright 2021 bejson.com 
  */
package com.kth.safebox.bean;

/**
 * Auto-generated: 2021-02-20 17:14:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Bom {

    private String ticketNo;
    private String sn;
    private int type;
    private int status;
    public void setTicketNo(String ticketNo) {
         this.ticketNo = ticketNo;
     }
     public String getTicketNo() {
         return ticketNo;
     }

    public void setSn(String sn) {
         this.sn = sn;
     }
     public String getSn() {
         return sn;
     }

    public void setType(int type) {
         this.type = type;
     }
     public int getType() {
         return type;
     }

    public void setStatus(int status) {
         this.status = status;
     }
     public int getStatus() {
         return status;
     }

}