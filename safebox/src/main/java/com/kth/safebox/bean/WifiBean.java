package com.kth.safebox.bean;

import android.net.wifi.ScanResult;

public class WifiBean {
    private String wifiName;
    private String level;
    private String state;  //已连接  正在连接  未连接 三种状态
    private String capabilities;//加密方式
    private ScanResult scanResult;//原生的对象ScanResult
    public WifiBean(String wifiName, String level, String state
    ,String capabilities,ScanResult scanResult) {
        this.wifiName = wifiName;
        this.level = level;
        this.state = state;
        this.capabilities = capabilities;
        this.scanResult = scanResult;
    }
    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities;
    }

    public ScanResult getScanResult() {
        return scanResult;
    }

    public void setScanResult(ScanResult scanResult) {
        this.scanResult = scanResult;
    }

}
