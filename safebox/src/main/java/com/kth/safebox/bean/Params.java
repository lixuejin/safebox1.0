/**
  * Copyright 2021 bejson.com 
  */
package com.kth.safebox.bean;
import java.util.List;

/**
 * Auto-generated: 2021-02-20 17:14:47
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Params {

    private List<Bom> Bom;
    public void setBom(List<Bom> Bom) {
         this.Bom = Bom;
     }
     public List<Bom> getBom() {
         return Bom;
     }

}