package com.kth.safebox.bean;

public class MessageBean {
    private String mac;
    private String msg;

    public MessageBean(String msg, String mac) {
        this.msg = msg;
        this.mac = mac;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
